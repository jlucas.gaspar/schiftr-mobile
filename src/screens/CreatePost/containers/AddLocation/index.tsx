import React from 'react';
import { Header } from '../../../../components';
import { useCreatePostContext } from '../../context';
import { Container, LocationInput, Inputwrapper } from './styles';

export const AddLocation = () => {
  const { setLocation, location, setStep } = useCreatePostContext();

  return (
    <Container>
      <Header goBackFunction={() => setStep('finishCreate')} />

      <Inputwrapper>
        <LocationInput
          value={location}
          placeholder="Your photo location..."
          placeholderTextColor="#333"
          onChangeText={(text: string) => setLocation(text)}
        />
      </Inputwrapper>
    </Container>
  );
}