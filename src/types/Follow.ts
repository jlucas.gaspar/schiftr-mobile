import { Dates } from './helpers';

export namespace Follow {
  export namespace Model {
    export type Complete = Dates & {
      id: string;
      userFollowingId: string;
      userFollowedId: string;
      isActive: boolean;
    }
    export type WithoutId = Omit<Model.Complete, 'id'>;
    export type WithoutDates = Omit<Model.Complete, 'createdAt' | 'updatedAt' | 'deletedAt'>;
    export type WithoutDateAndId = Omit<WithoutDates, 'id'>;
  }

  export namespace Http {
    export namespace Request {
      export type Follow = Pick<Model.Complete, 'userFollowedId'>;
      export type Unfollow = Pick<Model.Complete, 'userFollowedId'>;
      export type EnsureIsFollowing = Pick<Model.Complete, 'userFollowedId'>;
      export type GetNumberOfFollowers = { userId: string; }
      export type GetNumberOfFollowing = { userId: string; }
    }

    export namespace Response {
      export type Follow = Model.Complete;
      export type Unfollow = Model.Complete | null;
      export type EnsureIsFollowing = Model.Complete | null;
      export type GetNumberOfFollowers = number;
      export type GetNumberOfFollowing = number;
    }
  }
}