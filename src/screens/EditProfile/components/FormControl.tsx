import React from 'react';
import { SvgProps } from 'react-native-svg';

import { Container, Size } from './FormControl.styes';

type Props = {
  icon: React.FC<SvgProps>;
}

export const FormControl: React.FC<Props> = ({ icon: Icon, children }) => {
  return (
    <Container>
      <Icon />
      <Size />
      {children}
    </Container>
  );
}