import 'react-native-gesture-handler';
import React from 'react';
import { ThemeProvider } from './src/styles';
import { RooutRouter } from './src/routes';
import { ReduxProvider } from './src/store';

export default function App() {
  return (
    <ReduxProvider>
      <ThemeProvider>
        <RooutRouter />
      </ThemeProvider>
    </ReduxProvider>
  );
}