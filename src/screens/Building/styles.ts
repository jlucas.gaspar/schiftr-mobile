import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { lighten } from 'polished';

export const Container = styled.View`
  flex: 1;
`;

export const Content = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const IconWrapper = styled.View`
  border-radius: ${RFValue(100)}px;
  height: ${RFValue(150)}px;
  width: ${RFValue(150)}px;
  background-color: ${({ theme }) => lighten(0.45, theme.colors.primary)};
  justify-content: center;
  align-items: center;
  margin-bottom: ${RFValue(30)}px;
`;

type TextProps = { size: number; }
export const Text = styled.Text<TextProps>`
  font-family: ${({ theme }) => theme.fonts.raleway.regular};
  font-size: ${(({ size }) => RFValue(size))}px;
  margin: ${RFValue(3)}px 0;
`;