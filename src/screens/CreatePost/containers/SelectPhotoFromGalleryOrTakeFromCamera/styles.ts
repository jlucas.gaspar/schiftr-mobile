import styled from 'styled-components/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Content = styled.View`
  margin: ${RFValue(20)}px;
  background-color: white;
  border-radius: ${RFValue(20)}px;
  padding: ${RFValue(35)}px;
  align-items: center;
  shadow-color: #000;
  shadow-offset: 0px 2px;
  shadow-opacity: 0.25;
  shadow-radius: 3px;
`;

export const Title = styled.Text`
  font-family: ${_ => _.theme.fonts.raleway.regular};
`;

export const ButtonContainer = styled(TouchableOpacity)`
  border-radius: ${RFValue(10)}px;
  padding: ${RFValue(10)}px ${RFValue(50)}px;
  justify-content: center;
  align-items: center;
  background-color: black;
  margin-top: ${RFValue(20)}px;
`;

export const ButtonText = styled.Text`
  font-family: ${_ => _.theme.fonts.raleway.semiBold};
  font-size: ${RFValue(15)}px;
  color: white;
`;