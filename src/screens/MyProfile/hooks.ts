import * as ImageManipulator from 'expo-image-manipulator';
import * as ImagePicker from 'expo-image-picker';
import { useCallback, useState, useEffect } from 'react';
import { Platform } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { userService } from '../../services/user';
import { RootState } from '../../store';
import { setUser } from '../../utils/asyncStorage';
import * as userActions from '../../store/actions/user';

export const useChangePhoto = () => {
  const { user } = useSelector((state: RootState) => state.user);
  const [isLoading, setLoading] = useState(false);
  const [photoUri, setPhotoUri] = useState(user.avatarImageUrl);
  const dispatch = useDispatch();

  const getMe = async () => {
    setLoading(true);
    const userFromApi = await userService.getMe({ userId: user.id });
    if (userFromApi) {
      dispatch(userActions.setUser(userFromApi));
      await setUser(userFromApi);
    }
    setLoading(false);
  }

  useEffect(() => {
    getMe();
  }, []);

  const handleSelectPhoto = useCallback(async () => {
    if (Platform.OS !== 'web') {
      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.001
    });

    const { cancelled } = result;

    if (cancelled) {
      return;
    }

    const compressedResult = await ImageManipulator.manipulateAsync(result.uri, [], {
      compress: 0.1
    });

    setPhotoUri(compressedResult.uri);

    setLoading(true);

    const fileUrl = await userService.uploadPhoto({ fileUri: compressedResult.uri });
    if (!fileUrl) {
      return setLoading(false);
    }

    const user = await userService.updateUser({ avatarImageUrl: fileUrl });

    if (!user) {
      return setLoading(false);
    }

    dispatch(userActions.setUser(user));

    setLoading(false);
  }, [Platform, ImagePicker]);

  return {
    isLoading,
    handleSelectPhoto,
    photoUri
  }
}