import React from 'react';
import { Control, FieldValues, DeepMap, FieldError } from 'react-hook-form';
import { Form, Input } from './Form.styles';

type Props = {
  control: Control<FieldValues>;
  errors: DeepMap<FieldValues, FieldError>;
}

export const ChangePasswordForm = ({ control, errors }: Props) => {
  return (
    <Form>
        <Input
          control={control}
          errors={errors}
          name="oldPassword"
          placeholder="Current Password"
          autoCompleteType="password"
          textContentType="newPassword"
          placeholderTextColor="#e9e9e9"
          borderBottomColor="#e9e9e9"
        />

        <Input
          control={control}
          errors={errors}
          name="newPassword"
          placeholder="New Password"
          autoCompleteType="password"
          textContentType="newPassword"
          placeholderTextColor="#e9e9e9"
          borderBottomColor="#e9e9e9"
        />

        <Input
          control={control}
          errors={errors}
          name="newPasswordConfirmation"
          placeholder="New Password Confirmation"
          autoCompleteType="password"
          textContentType="newPassword"
          placeholderTextColor="#e9e9e9"
          borderBottomColor="#e9e9e9"
        />
    </Form>
  );
}