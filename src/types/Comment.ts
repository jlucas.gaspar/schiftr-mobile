import { Dates, Id } from './helpers';

export namespace Comment {
  export namespace Model {
    export type Complete = Dates & Id & {
      postId: string;
      createdAt: string;
      commentString: string;
      commentUserAvatar: string;
      commentUserUsername: string;
      answeredCommentId?: string;
      edited: boolean;
      usersLikedIdArray: string[];
    }
  }

  export namespace FormData {
  }

  export namespace Http {
    export namespace Request {
      export type AddLike = Pick<Model.Complete, 'id'>;
      export type RemoveLike = Pick<Model.Complete, 'id'>;
    }
    export namespace Response {
      export type AddLike = Model.Complete | null;
      export type RemoveLike = Model.Complete | null;
    }
  }

  export namespace Helpers {
  }
}