import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { RoutesList } from '../../constants';
import * as ComponentScreen from '../../../screens';

const Stack = createStackNavigator();

export const PublicRoutes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerMode: 'none' as any }} >
        <Stack.Screen
          name={RoutesList.GetStarted}
          component={ComponentScreen.GetStarted}
        />

        <Stack.Screen
          name={RoutesList.SignIn}
          component={ComponentScreen.SignIn}
        />

        <Stack.Screen
          name={RoutesList.SignUp}
          component={ComponentScreen.SignUp}
        />

        <Stack.Screen
          name={RoutesList.ForgotPassword}
          component={ComponentScreen.ForgotPassword}
        />

        <Stack.Screen
          name={RoutesList.ResetPassword}
          component={ComponentScreen.ResetPassword}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}