import 'styled-components';
import theme from '../styles/ThemeProvider/theme';

declare module 'styled-components' {
  type Theme = typeof theme;

  export interface DefaultTheme extends Theme {}
}