import React from 'react';
import { SvgProps } from 'react-native-svg';
import { RectButtonProps } from 'react-native-gesture-handler';
import { IconWrapper, StyledButton, Text } from './styles';

type Props = Omit<RectButtonProps, 'onPress'> & {
  text: string;
  icon?: React.FC<SvgProps>;
  onPress: ((data: any) => any) | any;
  isLoading?: boolean;
  color?: 'black' | 'white';
}

export const Button = ({ text, icon: Icon, onPress, color = 'white', isLoading, ...rest }: Props) => (
  <StyledButton onPress={onPress} enabled={!isLoading} {...rest} isLoading={isLoading || false} color={color} >
    {!isLoading && Icon && (
      <IconWrapper>
        <Icon />
      </IconWrapper>
    )}

    {!isLoading && <Text color={color}>{text}</Text>}
    {isLoading && <Text color={color}>Loading...</Text>}
  </StyledButton>
);