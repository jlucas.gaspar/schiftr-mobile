import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/core';
import { userService } from '../../services/user';
import { RootState } from '../../store';
import * as userActions from '../../store/actions/user';
import { editUserResolver } from './resolver';

type FormData = any;

export const useEditProfile = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { formState: { errors }, handleSubmit, control } = useForm({ resolver: editUserResolver });
  const dispatch = useDispatch();
  const { goBack } = useNavigation();

  const handleEditUser = handleSubmit(async (values: FormData) => {
    setIsLoading(true);

    const updatedUser = await userService.updateUser({
      name: values.name,
      birthday: values.birthday,
      gender: values.gender,
      country: values.country,
      city: values.city,
      state: values.state,
      zipCode: values.zipCode
    });

    if (!updatedUser) {
      setIsLoading(false);
      return;
    }

    dispatch(userActions.setUser(updatedUser));

    setIsLoading(false);

    goBack();
  });

  return { control, errors, handleEditUser, isLoading }
}