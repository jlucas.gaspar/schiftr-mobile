import { Post } from '../../types/Post';

type Actions = Post.Store.Actions.All;
type State = Post.Store.State;

const initialState: State = {
  profileFeed: [],
  feed: [],
  detailedPost: {} as Post.Model.Complete
}

export const postReducer = (state = initialState, action: Actions): State => {
  switch (action.type) {
    case Post.Store.ActionsName.SetFeed:
      return { ...state, feed: action.payload };

    case Post.Store.ActionsName.SetProfileFeed:
      return { ...state, profileFeed: action.payload };

    case Post.Store.ActionsName.SetDetailedPost:
      return { ...state, detailedPost: action.payload };

    default:
      return state;
  }
}