import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Form = styled.View`
  margin-top: ${RFValue(30)}px;
  padding: 20px;
`;

type SpaceProps = { size: number };
export const Space = styled.View<SpaceProps>`
  height: ${_ => _.size}px;
`;