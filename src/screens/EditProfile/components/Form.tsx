import React from 'react';
import { useSelector } from 'react-redux';
import { Control, FieldValues, DeepMap, FieldError } from 'react-hook-form';
import { UserFilled, LocationFilled, PhoneIcon, EmailIcon } from '../../../assets/icons';
import { RootState } from '../../../store';
import { FormControl } from './FormControl';
import { Form, TextInput } from './Form.styles';

type Props = {
  control: Control<FieldValues>;
  errors: DeepMap<FieldValues, FieldError>;
}

export const EditUserForm = ({ control, errors }: Props) => {
  const { user } = useSelector((state: RootState) => state.user);

  return (
    <Form>
      <FormControl icon={UserFilled}>
        <TextInput
          control={control}
          errors={errors}
          defaultValue={user.name}
          name="name"
          placeholder="Full Name"
          autoCompleteType="name"
          textContentType="name"
          placeholderTextColor="#e9e9e9"
        />
      </FormControl>

      <FormControl icon={LocationFilled}>
        <TextInput
          control={control}
          errors={errors}
          defaultValue={user.city}
          name="city"
          placeholder="City"
          autoCompleteType="street-address"
          textContentType="addressCity"
          placeholderTextColor="#e9e9e9"
        />
      </FormControl>

      <FormControl icon={EmailIcon}>
        <TextInput
          control={control}
          errors={errors}
          defaultValue={user.email}
          value={`(Disabled) ${user.email}`}
          name="email"
          autoCompleteType="street-address"
          textContentType="addressCity"
          placeholderTextColor="#e9e9e9"
        />
      </FormControl>

      <FormControl icon={PhoneIcon}>
        <TextInput
          control={control}
          errors={errors}
          defaultValue={user.phone}
          name="phone"
          keyboardType="phone-pad"
          placeholder="Phone"
          autoCompleteType="tel"
          textContentType="telephoneNumber"
          placeholderTextColor="#e9e9e9"
        />
      </FormControl>
    </Form>
  );
}