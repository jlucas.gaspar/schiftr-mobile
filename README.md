# How to build
 0. Run `npm -g expo-cli`.
 1. Ensure that you're logged with Schiftr account in expo. If not, run `expo login` and provide the correct e-mail and password.
 2. Run `expo publish`.
 3. You'll receive the QR Code in a link that you need to open on your phone. 