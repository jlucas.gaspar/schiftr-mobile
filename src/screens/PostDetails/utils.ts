import * as fns from 'date-fns';

export const parseCreationDateToString = (creationDate: string) => {
  const now = new Date();
  const creation = new Date(creationDate);

  const hoursDiff = fns.differenceInHours(now, creation);

  if (hoursDiff < 1) {
    const minutesDiff = fns.differenceInMinutes(now, creation);
    if (minutesDiff <= 1) return `${minutesDiff} minute`;
    else return `${minutesDiff} minutes`;
  }

  if (hoursDiff < 24) {
    if (hoursDiff <= 1) return `${hoursDiff} hour`;
    else return `${hoursDiff} hours`;
  }

  if (hoursDiff < 168) {
    const daysDiff = fns.differenceInDays(now, creation);
    if (daysDiff <= 1) return `${daysDiff} day`;
    else return `${daysDiff} days`;
  }

  const weeksDiff = fns.differenceInWeeks(now, creation);
  if (weeksDiff <= 1) return `${weeksDiff} week`;
  else return `${weeksDiff} weeks`;
}

export const parseLikesNumber = (likesNumber: number) => {
  if (likesNumber <= 999) return `${likesNumber}`;
  if (likesNumber <= 999999) return `${likesNumber / 1000}k`;
  if (likesNumber > 9999999) return '+10M';

  const [millionsNumber, thousandsNumber] = `${likesNumber / 1000000}`.split('.');
  if (!thousandsNumber) return `${millionsNumber}M`;

  const [firstThousand, secondThousand] = `${thousandsNumber}`.split('');
  return `${millionsNumber}.${firstThousand}${secondThousand}M`
}