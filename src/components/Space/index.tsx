import React from 'react';
import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

type SpaceProps = {
  size: number;
}

const CustomizableSpace = styled.View<SpaceProps>`
  height: ${({ size }) => RFValue(size)}px;
`;

export const Space: React.FC<SpaceProps> = ({ size }) => (
  <CustomizableSpace size={size} />
);