import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 8px 15px;
`;

export const Size = styled.View`
  width: 20px;
`;