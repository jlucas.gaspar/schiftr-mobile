import { SchemaOf, string, object } from 'yup';
import { Auth } from '../../types/Auth';

type FormData = Auth.FormData.EmailSignIn;

export const signInSchema: SchemaOf<FormData> = object().shape({
  email: string().required('E-mail is required.').email('Invalid e-mail'),
  password: string().required('Password is required.')
});