import { makeApiRequest } from './helpers/axios';
import { User } from '../types/User';
import { handleServiceError } from './helpers/errorHandler';

type UserService = {
  updateUser: User.Api.UpdateUser;
  uploadPhoto: User.Api.UploadPhoto;
  getMe: User.Api.GetMe;
  getUserByUsername: User.Api.GetUserByUsername;
}

export const userService: UserService = {
  updateUser: async (data) => {
    try {
      return await makeApiRequest({
        method: 'PUT',
        url: '/user',
        data
      });
    } catch (err) {
      handleServiceError(err, 'updateUser');
    }
  },

  uploadPhoto: async ({ fileUri }) => {
    try {
      const s3PreSignedUrl = await makeApiRequest({ method: 'GET', url: '/user/upload-url' });

      const xhr = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) { } // console.log('Upload OK');
          else throw new Error('Upload Fail');
        }
      }
      xhr.open('PUT', s3PreSignedUrl);
      const fileType = 'image/jpeg';
      xhr.setRequestHeader('Content-Type', fileType);
      xhr.send({ uri: fileUri, type: fileType });
      return s3PreSignedUrl.split('?')[0];
    } catch (error) {
      handleServiceError(error, 'uploadPhoto');
    }
  },

  getMe: async ({ userId }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/user/${userId}`
      });
    } catch (err) {
      handleServiceError(err, 'getMe');
    }
  },

  getUserByUsername: async ({ username }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/user/by-username/${username}`
      });
    } catch (err) {
      handleServiceError(err, 'getMe');
    }
  }
}