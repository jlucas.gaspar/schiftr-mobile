import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { forgotPasswordBackground } from '../../assets/images';

export const Container = styled.View`
  width: 100%;
  height: 100%;
  background-color: ${_ => _.theme.colors.background};
  color: ${_ => _.theme.colors.text};
`;

export const Title = styled.Text`
  margin-top: 40px;
  color: ${_ => _.theme.colors.text};
  font-size: ${RFValue(24)}px;
  font-family: ${_ => _.theme.fonts.nunito.medium};
`;

export const Description = styled.Text`
  margin-top: 15px;
  color: ${_ => _.theme.colors.text};
  font-size: ${RFValue(16)}px;
  font-family: ${_ => _.theme.fonts.nunito.regular};
`;

export const Content = styled.View`
  padding: 30px;
`;

export const ForgotPasswordBackground = styled.ImageBackground.attrs({ source: forgotPasswordBackground })`
  z-index: -50;
  margin-top: -50px;
  height: 500px;
`;