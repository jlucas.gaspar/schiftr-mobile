import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

export const Container = styled.TouchableOpacity``;

export const Image = styled.Image`
  width: ${(Dimensions.get('screen').width / 3) - 2}px;
  background-color: gray;
  height: ${(Dimensions.get('screen').width / 3) - 2}px;
  margin: 1px;
`;