import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { RectButton } from 'react-native-gesture-handler';

export const Container = styled.ScrollView``;

export const Image = styled.Image`
  height: ${RFValue(320)}px;
`;

export const ClickableContainer = styled.View`
  height: ${RFValue(320)}px;
  align-content: center;
  align-items: center;
  justify-content: center;
`;

export const SelectButton = styled(RectButton)`
  border-radius: 100px;
  height: 200px;
  width: 200px;
  background-color: #D2D2D2;
  align-items: center;
  justify-content: center;
`;