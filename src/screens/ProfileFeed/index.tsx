import React from 'react';
import { FlatList } from 'react-native-gesture-handler';
import { userWithoutAvatarUrl } from '../../assets/images';
import { Header, Loading, Space } from '../../components';

import { Photo } from './components/Photo';
import { useProfileFeed } from './hooks';
import {
  Container, UserAvatar, ActionButtonText, Username, UserInfoWrapper, ActionButton, Info,
  FollowingFollowersNumber, FollowingFollowersText, FollowingFollowersWrapper, PhotosWrapper
} from './styles';

type Props = {
  route: {
    params: {
      username: string;
    }
  }
}

export const ProfileFeed = ({ route }: Props) => {
  const { username } = route.params;
  const { isLoading, posts, user, handleFollow, myUsername } = useProfileFeed(username);

  if (isLoading) return <Loading />;
  return (
    <Container>
      <Header hasShadow />

      <Space size={50} />

      <UserAvatar
        source={{ uri: user?.avatarImageUrl ? user.avatarImageUrl : userWithoutAvatarUrl }}
      />

      <Username>{user?.username}</Username>

      <UserInfoWrapper>
        {myUsername !== username && (
          <Info>
            <ActionButton isFollowing={user?.youFollow || false} onPress={() => handleFollow(!user?.youFollow)}>
              <ActionButtonText>
                {user?.youFollow ? 'Unfollow' : 'Follow'}
              </ActionButtonText>
            </ActionButton>
          </Info>
        )}

        <Info>
          <FollowingFollowersNumber>{user?.followingNumber}</FollowingFollowersNumber>
          <FollowingFollowersText>Following</FollowingFollowersText>
        </Info>

        <Info>
          <FollowingFollowersNumber>{user?.followersNumber}</FollowingFollowersNumber>
          <FollowingFollowersText>Follower{user?.followersNumber && user?.followersNumber > 1 ? 's' : ''}</FollowingFollowersText>
        </Info>
      </UserInfoWrapper>

      <PhotosWrapper>
        <FlatList
          data={posts}
          keyExtractor={post => post.id}
          renderItem={({ item }) => <Photo id={item.id} imageUri={item.imageUri} />}
          showsVerticalScrollIndicator={false}
          numColumns={3}
        />
      </PhotosWrapper>
    </Container>
  );
}