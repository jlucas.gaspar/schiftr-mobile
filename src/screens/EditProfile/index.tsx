import React from 'react';
import { Header, Space } from '../../components';
import { Container, Title, Form, SaveButton } from './styles';
import { EditUserForm } from './components';
import { useEditProfile } from './hooks';

export const EditProfile: React.FC = () => {
  const { control, errors, handleEditUser, isLoading } = useEditProfile();

  return (
    <>
      <Header hasShadow />

      <Container>
        <Space size={50} />

        <Title>Edit Profile</Title>

        <Form>
          <EditUserForm control={control} errors={errors} />
        </Form>

        <SaveButton text="SAVE" onPress={handleEditUser} isLoading={isLoading} />
      </Container>
    </>
  );
}