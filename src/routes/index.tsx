import React from 'react';
import { Loading } from '../components';
// import { PrivateRoutes, PublicRoutes } from './components';
import { PrivateRoutes } from './components/PrivateRoutes';
import { PublicRoutes } from './components/PublicRoutes';
import { useRooutRouter } from './hooks';

export const RooutRouter = () => {
  const {
    isAuthenticated,
    isLoading
  } = useRooutRouter();

  if (isLoading)
    return <Loading />;
  else
    if (isAuthenticated) return <PrivateRoutes />;
    else return <PublicRoutes />;
}