import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Form = styled.View`
  width: 100%;
`;

export const Container = styled.View`
  padding: 20px;
  height: 100%;
  background-color: ${_ => _.theme.colors.background};
`;

export const Text = styled.Text`
  margin: 50px 0;
  font-size: ${RFValue(20)}px;
  color: ${_ => _.theme.colors.text};
  font-family: ${_ => _.theme.fonts.nunito.medium};
`;