export default {
  colors: {
    primary: '#D91A5E',

    success: '#12A454',
    success_light: 'rgba(18, 164, 84, 0.5)',

    error: '#E83F5B',
    error_light: 'rgba(232, 63, 91, 0.5)',

    text: '#FFFFFF',
    text_light: '#707070',

    background: '#000000',
    green: '#57A2A0'
  },

  fonts: {
    nunito: {
      regular: 'NunitoSans_400Regular',
      medium: 'NunitoSans_600SemiBold',
      bold: 'NunitoSans_700Bold'
    },

    raleway: {
      regular: 'Raleway_400Regular',
      semiBold: 'Raleway_600SemiBold'
    }
  }
}