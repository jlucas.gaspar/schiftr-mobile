import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigation } from '@react-navigation/native';
import { yupResolver } from '@hookform/resolvers/yup';
// import { authService } from '../../services/auth';
// import { StackRoutes } from '../../routes/types';
import { forgotPasswordSchema } from './schema';
import { Auth } from '../../types/Auth';

type FormData = Auth.FormData.ForgotPassword;

const resolver = yupResolver(forgotPasswordSchema);

export const useForgotPassword = () => {
  const [isLoading, setLoading] = useState(false);
  const { navigate } = useNavigation();
  const { formState: { errors }, handleSubmit, control } = useForm({ resolver });

  const handleForgotPassword = handleSubmit(async ({ email }: FormData) => {
    // setLoading(true);

    // const response = await authService.forgotPassword({ email });

    // console.log('resetPassword Result: ', response); // TODO :: remover esse cara

    // if (!response.isOk) {
    //   //* Fazer um toast aqui
    //   console.error(response.errors);
    //   setLoading(false);
    //   return;
    // }

    // setLoading(false);
    // navigate(StackRoutes.ResetPassword);
  });

  return { control, errors, handleForgotPassword, isLoading }
}