import LogoIcon from './logo.svg';
import GoogleIcon from './google.svg';
import LogoIconLightMode from './logo-light-mode.svg';
import EditUserIcon from './edit-user.svg';
import LockIcon from './lock.svg';
import HeartIcon from './heart.svg';
import HeartFilledIcon from './heart-filled.svg';
import CommentIcon from './comment.svg';
import EmailIcon from './email.svg';
import PhoneIcon from './phone.svg';
import UserFilled from './user-filled.svg';
import LocationFilled from './location-filled.svg';

export {
  LogoIcon,
  LogoIconLightMode,
  EditUserIcon,
  GoogleIcon,
  HeartIcon,
  HeartFilledIcon,
  CommentIcon,
  LockIcon,
  UserFilled,
  LocationFilled,
  EmailIcon,
  PhoneIcon
}