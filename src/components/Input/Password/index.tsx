import React, { useState } from 'react';
import { Feather } from '@expo/vector-icons';
import { TextInputProps } from 'react-native';
import { Control, Controller, DeepMap, FieldValues, FieldError } from 'react-hook-form';
import { StyledInput, Container, Error, StyledView } from './styles';

type InputProps = TextInputProps & {
  control: Control;
  name: string;
  errors: DeepMap<FieldValues, FieldError>;
  borderBottomColor?: string;
}

export const PasswordInput: React.FC<InputProps> = ({ control, name, errors, borderBottomColor, ...rest }) => {
  const [showPassword, setShowPasword] = useState(false);

  return (
    <Container>
      <Controller
        control={control}
        name={name}
        render={({ field: { onChange, value } }) => (
          <StyledView borderBottomColor={borderBottomColor}>
            <StyledInput
              onChangeText={onChange}
              secureTextEntry={!showPassword}
              value={value}
              placeholderTextColor="#999"
              {...rest}
            />
            <Feather
              name={showPassword ? "eye-off" : "eye"}
              size={20}
              style={{ color: 'white' }}
              onPress={() => setShowPasword(!showPassword)}
            />
          </StyledView>
        )}
      />
    </Container>
  );
}