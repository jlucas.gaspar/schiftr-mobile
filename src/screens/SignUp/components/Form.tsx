import React from 'react';
import { Control, FieldValues, DeepMap, FieldError } from 'react-hook-form';
import { GoogleIcon } from '../../../assets/icons';
// import { Input, Button, Checkbox, Divider, PasswordInput } from '../../../components';
import { Input, Button, Divider, PasswordInput } from '../../../components';
import { Form, Space } from './Form.styles';

type Props = {
  isLoading: boolean;
  isLoadingGoogle: boolean;
  googleSubmit: () => any;
  emailSubmit: () => any;
  control: Control<FieldValues>;
  errors: DeepMap<FieldValues, FieldError>
}

export const SignUpForm = (props: Props) => {
  const { control, isLoading, errors, googleSubmit, emailSubmit, isLoadingGoogle } = props;

  return (
    <Form>
      <Input
        placeholder="E-mail"
        name="email"
        control={control}
        errors={errors}
        keyboardType="email-address"
        autoCompleteType="email"
        textContentType="emailAddress"
      />

      <Space size={15} />
      <Input
        placeholder="Username"
        name="username"
        control={control}
        errors={errors}
        textContentType="username"
        autoCompleteType="username"
      />

      <Space size={15} />
      <PasswordInput
        placeholder="Password"
        name="password"
        control={control}
        errors={errors}
      />

      <Space size={15} />
      <PasswordInput
        placeholder="Password confirmation"
        name="passwordConfirmation"
        control={control}
        errors={errors}
      />

      {/* <Checkbox text="You agree the terms and privacy policy" isChecked={agree} onValueChange={isChecked => setAgree(isChecked)} /> */}
      <Space size={20} />

      <Button text="SIGN UP" onPress={emailSubmit} isLoading={isLoading} />
      <Divider text="OR" />
      {/* <Button icon={GoogleIcon} text="SIGN UP WITH GOOGLE" onPress={googleSubmit} isLoading={isLoadingGoogle} /> */}
      <Button icon={GoogleIcon} text="SIGN UP WITH GOOGLE" onPress={() => {}} isLoading={isLoadingGoogle} />
    </Form>
  );
}