import React from 'react';
import { Header } from '../../../components';
import { Step } from '../hooks';
import { Container, LocationInput, Inputwrapper } from './AddLocation.styles';

type Props = {
  setLocation: (location: any) => any;
  setStep: React.Dispatch<React.SetStateAction<Step>>;
  location?: string;
}

export const AddLocation = ({ setLocation, location, setStep }: Props) => {
  return (
    <Container>
      <Header goBackFunction={() => setStep('create')} />

      <Inputwrapper>
        <LocationInput
          value={location}
          placeholder="Your photo location..."
          placeholderTextColor="#333"
          onChangeText={(text: string) => setLocation(text)}
        />
      </Inputwrapper>
    </Container>
  );
}