import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';
import { Feather } from '@expo/vector-icons';
import { getStatusBarHeight } from 'react-native-iphone-x-helper';
import { LogoIcon as Logo } from '../../../assets/icons';

export const Container = styled.View`
  width: 100%;
  margin-top: ${getStatusBarHeight()}px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const BackButton = styled(RectButton)`
  width: 100%;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  margin-left: 25px;
`;

export const BackIcon = styled(Feather)`
  color: ${_ => _.theme.colors.text};
  font-size: ${RFValue(16)}px;
  font-weight: 800;
  margin-right: 10px;
`;

export const TextButton = styled.Text`
  color: ${_ => _.theme.colors.text};
  font-family: ${_ => _.theme.fonts.nunito.bold};
  font-size: ${RFValue(14)}px;
`;

export const LogoIcon = styled(Logo).attrs({ width: 150 })``;

export const Space = styled.View`
  width: 60px;
`;