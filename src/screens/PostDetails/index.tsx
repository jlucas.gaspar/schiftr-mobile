import React from 'react';
import { FlatList } from 'react-native-gesture-handler';
import { Header, Loading, Space, PostCard } from '../../components';
import { Post } from '../../types/Post';
import { Comment } from './components';
import { usePostDetails } from './hooks';
import { Container, Divider } from './styles';

type PostDetailsProps = {
  route: {
    params: {
      postId: string;
    }
  }
}

type CommentFlatList = {
  item: Post.Helpers.Comment;
}

export const PostDetails = ({ route }: PostDetailsProps) => {
  const { isLoading, post } = usePostDetails(route.params.postId);

  if (isLoading) return <Loading />;
  if (!post) return <Loading />;
  // if (!user) return;
  return (
    <>
      <Header hasShadow />

      <Container>
        <Space size={35} />

        <PostCard data={post} postOption="detailedPost">
          <Divider />

          <FlatList
            data={post.comments}
            keyExtractor={(comment: Post.Helpers.Comment) => comment.id}
            renderItem={({ item: comment }: CommentFlatList) => (
              <Comment
                commentString={comment.commentString}
                commentUserUsername={comment.commentUserUsername}
                commentUserAvatar={comment.commentUserAvatar}
                createdAt={comment.createdAt}
                usersLikedIdArray={comment.usersLikedIdArray}
                id={comment.id}
              />
            )}
          />
        </PostCard>
      </Container>
    </>
  );
}