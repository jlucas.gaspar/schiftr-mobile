import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
  margin: 5px 30px;
`;

type LineProps = { lineColor?: string }
export const Line = styled.View<LineProps>`
  flex: 1;
  height: 1.5px;
  background-color: ${({ lineColor, theme }) => lineColor ? lineColor : theme.colors.text_light};
`;

export const TextWrapper = styled.View``;

export const Text = styled.Text`
  margin: 0 10px;
  text-align: center;
  color: ${_ => _.theme.colors.text};
  font-family: ${_ => _.theme.fonts.nunito.regular};
  font-size: ${RFValue(15)}px;
`;