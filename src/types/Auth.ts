import { User } from './User';
import { MakeFunction } from './helpers';

export namespace Auth {
  export namespace FormData {
    export type ForgotPassword = Pick<User.Model.Complete, 'email'>;
    export type ResetPassword = Pick<User.Model.Complete, 'email'>;
    export type EmailSignIn = { email: string; password: string; };
    export type EmailSignUp = EmailSignIn & { username: string; passwordConfirmation: string; };
    export type UpdatePassword = { password: string; passwordConfirmation: string; }
  }

  export namespace Api {
    export type GoogleSignUp = MakeFunction<Req.GoogleSignUp, Res.GoogleSignUp>;
    export type GoogleSignIn = MakeFunction<Req.GoogleSignIn, Res.GoogleSignIn>;
    export type EmailSignIn = MakeFunction<Req.EmailSignIn, Res.EmailSignIn>;
    export type EmailSignUp = MakeFunction<Req.EmailSignUp, Res.EmailSignUp>;
    export type RefreshToken = MakeFunction<Req.RefreshToken, Res.RefreshToken>;
    export type ResetPassword = MakeFunction<Req.ResetPassword, Res.ResetPassword>;
    export type ForgotPassword = MakeFunction<Req.ForgotPassword, Res.ForgotPassword>;
    export type ConfirmResetPasswordReceivedToken = MakeFunction<Req.ConfirmResetPasswordReceivedToken, Res.ConfirmResetPasswordReceivedToken>;
    export type UpdatePassword = MakeFunction<Req.UpdatePassword, Res.UpdatePassword>;

    namespace Req { // Request
      export type RefreshToken = { userId: string }
      export type GoogleSignUp = void;
      export type GoogleSignIn = void;
      export type EmailSignIn = FormData.EmailSignIn;
      export type EmailSignUp = FormData.EmailSignUp;
      export type ResetPassword = FormData.ResetPassword;
      export type ForgotPassword = FormData.ForgotPassword;
      export type ConfirmResetPasswordReceivedToken = { token: string; }
      export type UpdatePassword = FormData.UpdatePassword & { token: string | number; }
    }
    namespace Res { // Response
      export type RefreshToken = Helpers.JwtTokenWithExp; // | void;
      export type GoogleSignUp = Helpers.UserWithToken; // | void;
      export type GoogleSignIn = Helpers.UserWithToken; // | void;
      export type EmailSignIn = Helpers.UserWithToken; // | void;
      export type EmailSignUp = Helpers.UserWithToken; // | void;
      export type ResetPassword = string; // | void;
      export type ForgotPassword = string; // | void;
      export type ConfirmResetPasswordReceivedToken = string; // | void;
      export type UpdatePassword = Helpers.UserWithToken; // | void;
    }
  }

  export namespace Helpers {
    export type JwtTokenWithExp = { token: string; exp: number; }
    export type UserWithToken = { user: User.Model.Complete; token: Helpers.JwtTokenWithExp; };
  }
}