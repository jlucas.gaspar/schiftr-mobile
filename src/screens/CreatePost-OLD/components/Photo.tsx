import React from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Container, ClickableContainer, Image, SelectButton } from './Photo.styles';

type Props = {
  photoUri?: string;
  onSelectImage: () => any;
}

export const Photo = ({ photoUri, onSelectImage }: Props) => {
  return (
    <Container>
      {photoUri && (<Image source={{ uri: photoUri }} />)}

      {!photoUri && (
        <ClickableContainer>
          <SelectButton onPress={onSelectImage}>
            <MaterialCommunityIcons name="camera-plus" size={80} />
          </SelectButton>
        </ClickableContainer>
      )}
    </Container>
  );
}