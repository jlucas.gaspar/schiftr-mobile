import { Post } from '../../types/Post';

type Model = Post.Model.Complete;

type SetFeed = Post.Store.Actions.SetFeed;
type SetProfileFeed = Post.Store.Actions.SetProfileFeed;
type SetDetailedPost = Post.Store.Actions.SetDetailedPost;

export const setFeed = (posts: Model[]): SetFeed => ({
  type: Post.Store.ActionsName.SetFeed,
  payload: posts
});

export const setProfileFeed = (posts: Model[]): SetProfileFeed => ({
  type: Post.Store.ActionsName.SetProfileFeed,
  payload: posts
});

export const setDetailedPost = (post: Model): SetDetailedPost => ({
  type: Post.Store.ActionsName.SetDetailedPost,
  payload: post
});