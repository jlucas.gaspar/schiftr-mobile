import styled from 'styled-components/native';
import { PasswordInput } from '../../../components';

export const Form = styled.View``;

export const Input = styled(PasswordInput)`
  border-bottom-color: #FFF;
  border-color: #FFF;
`;