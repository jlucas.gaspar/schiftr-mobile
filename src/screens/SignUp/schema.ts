import { SchemaOf, string, object, ref } from 'yup';
import { Auth } from '../../types/Auth';

type FormData = Auth.FormData.EmailSignUp;

export const signUpSchema: SchemaOf<FormData> = object().shape({
  email: string().required('E-mail is required.').email('Invalid e-mail'),
  username: string().required('Username is required.'),
  password: string().required('Password is required.'),
  passwordConfirmation: string().required('Password confirmation is required.').oneOf([ref('password'), null], 'Passwords must match.')
});