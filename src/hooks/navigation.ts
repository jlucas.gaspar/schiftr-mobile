import * as reactNavigation from '@react-navigation/core';
import { RoutesList } from '../routes/constants';

export const useNavigation = () => {
  const nav = reactNavigation.useNavigation();

  const goBack = () => {
    if (nav.canGoBack()) return nav.goBack();
    else return nav.navigate(RoutesList.Feed as never);
  }

  const navigate = (routeName: RoutesList, options: any) => {
    return nav.navigate(routeName as never, options as never);
  }

  return {
    goBack,
    navigate
  }
}