import { BaseError } from './BaseError';

export class ApiError implements BaseError {
  constructor(
    public message: string,
    public type: string = 'Api Error',
    public isAppError: boolean = true
  ) {}
}

export class AsyncStorageError implements BaseError {
  constructor(
    public message: string,
    public type: string = 'Api Error',
    public isAppError: boolean = true
  ) {}
}

export class EnvError implements BaseError {
  constructor(
    public message: string,
    public type: string = 'Env Error',
    public isAppError: boolean = true
  ) {}
}

export class AppError implements BaseError {
  constructor(
    public message: string,
    public type: string = 'App Error',
    public isAppError: boolean = true
  ) {}
}