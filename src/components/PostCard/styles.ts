import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const PostInfo = styled.View`
  flex-direction: row;
  margin: 3px 15px;
`;

export const UserInfoWrapper = styled.TouchableOpacity`
  flex-direction: row;
`;

export const UsernameAndLocation = styled.View`
  justify-content: center;
`;

export const UserAvatar = styled.Image`
  width: ${RFValue(32)}px;
  height: ${RFValue(32)}px;
  border-radius: 100px;
  margin-right: 10px;
`;

export const Username = styled.Text`
  font-family: ${({ theme }) => theme.fonts.nunito.regular};
  font-size: ${RFValue(13)}px;
`;

export const PostLocation = styled.Text`
  font-family: ${({ theme }) => theme.fonts.nunito.regular};
  font-size: ${RFValue(12)}px;
  color: #444;
`;

export const PostImage = styled.Image`
  height: ${RFValue(320)}px;
`;

export const LikesAndCreation = styled.View`
margin: 3px 15px;
  flex-direction: row;
  justify-content: space-between;
`;

export const CreationText = styled.Text`
  font-family: ${({ theme }) => theme.fonts.nunito.regular};
  color: #333;
`;

export const LikesCounter = styled.Text`
  flex-direction: row;
  font-family: ${({ theme }) => theme.fonts.nunito.regular};
  font-size: ${RFValue(13)}px;
`;

export const LikesStrong = styled.Text`
  font-family: ${({ theme }) => theme.fonts.nunito.bold};
`;

export const PostCommentsAndDescription = styled.Text`
  flex-direction: row;
  align-items: center;
  margin: 3px 15px;
`;

export const PostCommentsUsername = styled.Text`
  font-size: ${RFValue(15)}px;
  font-family: ${({ theme }) => theme.fonts.nunito.bold};
`;

export const Description = styled.Text``;

export const CommentWrapper = styled.View`
  border: 1px black solid;
  border-radius: 10px;
  padding: 5px 10px;
  width: 90%;
  margin: auto;
  flex-direction: row;
`;

export const CommentInput = styled.TextInput`
  width: 95%;
`;