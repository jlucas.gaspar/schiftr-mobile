import React from 'react';
import AppLoading from 'expo-app-loading';
import { ThemeProvider as StyledThemeProvider } from 'styled-components';
import { useFonts, Raleway_600SemiBold, Raleway_400Regular } from '@expo-google-fonts/raleway';
import { NunitoSans_400Regular, NunitoSans_600SemiBold, NunitoSans_700Bold } from '@expo-google-fonts/nunito-sans';
import theme from './theme';

export const ThemeProvider: React.FC = ({ children }) => {
  const [allFontsAreLoaded] = useFonts({
    Raleway_400Regular,
    Raleway_600SemiBold,
    NunitoSans_400Regular,
    NunitoSans_600SemiBold,
    NunitoSans_700Bold
  });

  if (!allFontsAreLoaded) return <AppLoading />;
  return (
    <StyledThemeProvider theme={theme}>
      {children}
    </StyledThemeProvider>
  );
}