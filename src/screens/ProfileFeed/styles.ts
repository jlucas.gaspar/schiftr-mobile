import { RectButton } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';

type FollowButton = {
  isFollowing: boolean;
}

export const Container = styled.View`
  background-color: #FFF;
  flex: 1;
`;

export const UserAvatar = styled.Image`
  height: 120px;
  width: 120px;
  border-radius: 100px;
  margin: 0 auto;
`;

export const Username = styled.Text`
  margin: 15px auto;
  font-family: ${({ theme }) => theme.fonts.nunito.regular};
  font-size: ${RFValue(18)}px;
`;

export const UserInfoWrapper = styled.View`
  flex-direction: row;
  justify-content: space-around;
  width: 100%;
`;

export const Info = styled.View`
  width: 30%;
  margin: auto;
  justify-content: center;
  align-items: center;
`;

export const ActionButton = styled(RectButton)<FollowButton>`
  border-radius: 10px;
  background-color: ${({ isFollowing, theme }) => isFollowing ? theme.colors.green : theme.colors.primary};
  padding: 8px 15px;
`;

export const ActionButtonText = styled.Text`
  font-family: ${({ theme }) => theme.fonts.nunito.bold};
  color: #FFF;
`;

export const FollowingFollowersWrapper = styled.View`
  justify-content: center;
  align-items: center;
  background-color: yellow;
`;
export const FollowingFollowersNumber = styled.Text`
  /* margin: auto;
  font-family: ${({ theme }) => theme.fonts.nunito.regular}; */
`;

export const FollowingFollowersText = styled.Text`
  /* margin: auto;
  font-family: ${({ theme }) => theme.fonts.nunito.regular}; */
`;


export const PhotosWrapper = styled.View`
  flex-direction: column;
  margin-top: ${RFValue(20)}px;
  flex-wrap: wrap;
`;