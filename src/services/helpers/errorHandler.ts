import axios from 'axios';
import { Alert } from 'react-native';
import { BaseError } from '../../errors/BaseError';

export const handleServiceError = (err: any, functionName: string): void => {
  const date = new Date().toLocaleString('pt-br');

  if (axios.isAxiosError(err)) {
    const data = err.response?.data;

    if (Number(data.statusCode) < 500 && typeof data.data === 'string') {
      Alert.alert(data.data);
    } else if (Number(data.statusCode) < 500 && data.data && typeof data.data === 'object') {
      Alert.alert(Object.values(data.data)[0] as string);
    } else {
      console.log(`
        [Date: ${date}] \n
        [FunctionName: ${functionName}] \n
        Axios Error. Details: ${err} \n
        - - - - - - - - - - - - - - -`
      );
      Alert.alert('Server Error. Please see your log');
    }
    return;
  }

  if (err instanceof BaseError) {
    Alert.alert(err.message);
    return console.warn(`
      [Date: ${date}] \n
      [FunctionName: ${functionName}] \n
      App Error. Details: ${err} \n
      - - - - - - - - - - - - - - -`
    );
  }

  console.error(`
    [Date: ${date}] \n
    [FunctionName: ${functionName}] \n
    Unknown Error. Details: ${err} \n
    - - - - - - - - - - - - - - -`
  );
  Alert.alert('Unexpected Error. Please see your log.');
}