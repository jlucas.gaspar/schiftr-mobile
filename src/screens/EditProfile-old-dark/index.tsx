import React from 'react';
import { Header, Space } from '../../components';
import { Container, BikeBackground, Form } from './styles';
import { EditUserForm } from './components';
import { useEditProfile } from './hooks';

export const EditProfile = () => {
  const { control, errors, handleEditUser, isLoading } = useEditProfile();

  return (
    <>
      <Header actionText="SAVE" hasShadow={false} />

      <Container>
        <Space size={20} />

        <Form>
          <EditUserForm
            control={control}
            errors={errors}
            onSubmit={handleEditUser}
            isLoading={isLoading}
          />
        </Form>

        <BikeBackground />
      </Container>
    </>
  );
}