import { useState, useEffect } from 'react';
import { NativeScrollEvent, NativeSyntheticEvent } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../store';
import { postService } from '../../services/post';
import * as postActions from '../../store/actions/post';

export const useFetchFeedPosts = () => {
  const [isLoading, setLoading] = useState(true);
  const { post } = useSelector((state: RootState) => state);
  const dispatch = useDispatch();

  useEffect(() => {
    fetchPosts();
  }, []);

  const fetchPosts = async () => {
    setLoading(true);
    const response = await postService.getAllPostsForUserFeed();
    if (response) {
      dispatch(postActions.setFeed([...response]));
    }
    setLoading(false);
  }

  const handleScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    // // ver o event.nativeEvent.contentSize que da o height da FlatList;
    // // Contar quantos em y pra cima a tela foi. Se aproximar do contentSize aí chama um loadMore;
    // console.warn(event.nativeEvent.contentSize);
    // console.log(event.nativeEvent.contentOffset);

    // // contentOffset + 750 vai dar "contentSize";
    // // A diferença desses 2 é 755;
  }

  return {
    isLoading,
    fetchPosts,
    posts: post.feed,
    handleScroll
  }
}