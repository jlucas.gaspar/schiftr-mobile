import React from 'react';
import { SelectPhotoFromGalleryOrTakeFromCamera } from '../containers/SelectPhotoFromGalleryOrTakeFromCamera';
import { FinishCreation } from '../containers/FinishCreation';
import { AddLocation } from '../containers/AddLocation';
import { useCreatePostContext } from '../context';
import { Container } from './styles';

export const CreatePostView = () => {
  const { step } = useCreatePostContext();

  return (
    <Container>
      {step === 'selectPhotoFromGalleryOrTakeFromCamera' && (
        <SelectPhotoFromGalleryOrTakeFromCamera />
      )}

      {step === 'finishCreate' && (
        <FinishCreation />
      )}

      {step === 'addLocation' && (
        <AddLocation />
      )}
    </Container>
  );
}