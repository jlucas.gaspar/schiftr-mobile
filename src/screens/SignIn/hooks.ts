import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { yupResolver } from '@hookform/resolvers/yup';
import { Auth } from '../../types/Auth';
// import { StackRoutes } from '../../routes/types';
import { authService } from '../../services/auth';
import * as userActions from '../../store/actions/user';
import { signInSchema } from './schema';
import { RoutesList } from '../../routes/constants';

type Form = Auth.FormData.EmailSignIn;
const resolver = yupResolver(signInSchema);

export const useSignIn = () => {
  const [rememberMe, setRememberMe] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const { formState: { errors }, handleSubmit, control } = useForm({ resolver });
  const { navigate } = useNavigation();
  const dispatch = useDispatch();

  const goToForgotPasswordScreen = () => navigate(RoutesList.ForgotPassword as any);

  const handleSignIn = handleSubmit(async ({ email, password }: Form) => {
    setLoading(true);

    const result = await authService.emailSignIn({ password, email });

    if (!result) {
      setLoading(false);
      return;
    }

    dispatch(userActions.authenticate(result));
    setLoading(false);
  });

  return { isLoading, errors, handleSignIn, control, rememberMe, setRememberMe, goToForgotPasswordScreen }
}