import React from 'react';

import { Component } from './components';
import { useBase } from './hooks';
import { Container } from './styles';

export const Base = () => {
  const { isLoading, handle } = useBase();

  return (
    <Container>
      <Component />
    </Container>
  );
}