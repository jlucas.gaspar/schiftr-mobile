import React from 'react';
import { View, KeyboardAvoidingView } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { AntDesign } from '@expo/vector-icons';
import { useSelector } from 'react-redux';
import { Zoomable } from '../Zoomable';
import { Post } from '../../types/Post';
import { RootState } from '../../store';
import { userWithoutAvatarUrl } from '../../assets/images';
import { HeartIcon, CommentIcon, HeartFilledIcon } from '../../assets/icons';
import { ensureUserLikedPost, parseCreationDateToString } from './utils';
import { usePostCard, PostOptions } from './hooks';
import {
  PostInfo, UserAvatar, UserInfoWrapper, Username, PostLocation,
  Description, CreationText, PostImage, LikesCounter, LikesStrong,
  PostCommentsAndDescription, PostCommentsUsername, LikesAndCreation,
  CommentWrapper, CommentInput, UsernameAndLocation
} from './styles';

type Data = {
  data: Post.Model.Complete;
  postOption: PostOptions;
}

export const PostCard: React.FC<Data> = ({ data, children, postOption }) => {
  const { user } = useSelector((state: RootState) => state.user);

  const {
    canComment,
    handleGoToProfile,
    setCanComment,
    handleAddLike,
    handleRemoveLike,
    handleAddComment,
    handleDeleteComment,
    setTextInputCommentString,
    textInputCommentString
  } = usePostCard({
    username: data.userUsername,
    postId: data.id
  });

  return (
    <KeyboardAvoidingView behavior="position">
      <PostInfo>
        <UserInfoWrapper onPress={handleGoToProfile}>
          <UserAvatar source={{ uri: data.userAvatar ? data.userAvatar : userWithoutAvatarUrl }} />
          <UsernameAndLocation>
            {!data.location && <Username>{data.userUsername}</Username>}
            {data.location && (
              <>
                <Username>{data.userUsername}</Username>
                <PostLocation>{data.location}</PostLocation>
              </>
            )}
          </UsernameAndLocation>
        </UserInfoWrapper>

        {/* Aqui vai os 3 pontinhos do lado direito */}
      </PostInfo>

      {data.imagesUrl.map((uri, key) => (
        <Zoomable key={key}>
          <PostImage source={{ uri }} />
        </Zoomable>
      ))}

      <LikesAndCreation>
        {data.likes.length === 0 && <LikesCounter>No likes yet</LikesCounter>}
        {data.likes.length !== 0 && (
          <LikesCounter>
            Liked by <LikesStrong>{data.likes.length}</LikesStrong> people
          </LikesCounter>
        )}
        <CreationText>{parseCreationDateToString(data.createdAt)}</CreationText>
      </LikesAndCreation>

      <PostCommentsAndDescription>
        {ensureUserLikedPost(data.likes, user.id)
          ? (
            <TouchableOpacity onPress={() => handleRemoveLike(postOption)}>
              <HeartFilledIcon />
            </TouchableOpacity>
          )
          : (
            <TouchableOpacity onPress={() => handleAddLike(postOption)}>
              <HeartIcon />
            </TouchableOpacity>
          )
        }

        <View style={{ width: 10 }} />

        <TouchableOpacity onPress={() => setCanComment(!canComment)}>
          <CommentIcon />
        </TouchableOpacity>

        <View style={{ width: 10 }} />

        <PostCommentsUsername onPress={handleGoToProfile}>
          {data.userUsername}
        </PostCommentsUsername>

        <View style={{ width: 10 }} />

        <Description>
          {data.text}
        </Description>
      </PostCommentsAndDescription>

      {canComment && (
        // <>
        <CommentWrapper>
          <CommentInput
            multiline
            onChangeText={text => setTextInputCommentString(text)}
            value={textInputCommentString}
          />

          <TouchableOpacity onPress={() => handleAddComment(postOption)}>
            <AntDesign name="pluscircle" size={20} color="black" />
          </TouchableOpacity>
        </CommentWrapper>
        // </>
      )}

      {children}
    </KeyboardAvoidingView>
  );
}