import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { yupResolver } from '@hookform/resolvers/yup';
import { useNavigation } from '@react-navigation/core';
import { userService } from '../../services/user';
import * as userActions from '../../store/actions/user';
import { schema, FormSchema } from './schema';

const resolver = yupResolver(schema);

export const useChangePasswordForm = () => {
  const [isLoading, setIsLoading] = useState(false);
  const { formState: { errors }, handleSubmit, control } = useForm({ resolver });
  const { goBack } = useNavigation();
  const dispatch = useDispatch();

  const handleUpdatePassword = handleSubmit(async ({ oldPassword, newPassword, newPasswordConfirmation }: FormSchema) => {
    setIsLoading(true);

    // const updatedUser = await userService.updateUser({
    //   name: values.name,
    //   birthday: values.birthday,
    //   gender: values.gender,
    //   country: values.country,
    //   city: values.city,
    //   state: values.state,
    //   zipCode: values.zipCode,
    //   phone: values.phone
    // });

    // if (!updatedUser) {
    //   setIsLoading(false);
    //   return;
    // }

    // dispatch(userActions.setUser(updatedUser));

    // setIsLoading(false);

    // goBack();
  });

  return { control, errors, handleUpdatePassword, isLoading }
}