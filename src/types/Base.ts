import { Dates } from './helpers';

export namespace Base {
  export namespace Model {
    export type Complete = Dates & {
    }
  }

  export namespace FormData {
  }

  export namespace Http {
    export namespace Req {
    }
    export namespace Res {
    }
  }

  export namespace Helpers {
  }
}