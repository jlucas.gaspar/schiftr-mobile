import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { RoutesList } from '../../../routes/constants';
import { PostCard } from '../../../components';
import { Post as PostType } from '../../../types/Post';
import { NumberOfComments, NumberOfCommentsWrap, Divider, DividerWrap } from './Post.style';

type Input = {
  postId: string;
}

type Props = {
  data: PostType.Model.Complete;
}

export const Post = ({ data }: Props) => {
  const { navigate } = useNavigation();

  const handleGoToPostDetails = () => {
    navigate(RoutesList.PostDetails as never, { postId: data.id } as never);
  }

  return (
    <PostCard postOption="feed" data={data}>
      <NumberOfCommentsWrap>
        <NumberOfComments onPress={handleGoToPostDetails}>
          {data.commentsNumber ? `See all ${data.commentsNumber} comments` : 'No comments yet'}
        </NumberOfComments>
      </NumberOfCommentsWrap>

      <DividerWrap>
        <Divider />
      </DividerWrap>
    </PostCard>
  )
}