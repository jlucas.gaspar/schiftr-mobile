import { Env } from './index';

export const env: Env = {
  apiUrl: '',
  iosClientId: '',
  androidClientId: ''
}