import { MakeFunction, Dates } from './helpers';

export namespace Post {
  export namespace Model {
    export type Complete = Dates & {
      id: string;
      userId: string;
      location?: string;
      imagesUrl: string[];
      text?: string;
      createdAt: Date;
      likes: string[];
      isPrivate: boolean;
      commentsNumber: number;
      comments: Helpers.Comment[];
      userAvatar: string;
      userUsername: string;
    }
  }

  export namespace FormData {
    export type Create = Pick<Model.Complete, 'location' | 'text' | 'imagesUrl' | 'isPrivate'>;
  }

  export namespace Api {
    export type Create = MakeFunction<Req.Create, Res.Create>;
    export type UploadPhoto = MakeFunction<Req.UploadPhoto, Res.UploadPhoto>;
    export type GetAllPostsOfAnUser = MakeFunction<Req.GetAllPostsOfAnUser, Res.GetAllPostsOfAnUser>;
    export type GetPostDetails = MakeFunction<Req.GetPostDetails, Res.GetPostDetails>;
    export type AddLike = MakeFunction<Req.AddLike, Res.AddLike>;
    export type RemoveLike = MakeFunction<Req.RemoveLike, Res.RemoveLike>;
    export type CreateComment = MakeFunction<Req.CreateComment, Res.CreateComment>;
    export type GetAllCommentsByPostId = MakeFunction<Req.GetAllCommentsByPostId, Res.GetAllCommentsByPostId>;
    export type GetAllPostsOfAnFeedUser = MakeFunction<Req.GetAllPostsOfAnFeedUser, Res.GetAllPostsOfAnFeedUser>;
    // Request
    namespace Req {
      export type Create = FormData.Create;
      export type UploadPhoto = { fileUri: string; };
      export type GetAllPostsOfAnUser = { userId: string; };
      export type GetPostDetails = { postId: string; };
      export type AddLike = { postId: string; };
      export type RemoveLike = { postId: string; };
      export type CreateComment = Pick<Helpers.Comment, 'answeredCommentId' | 'commentString' | 'postId'>;
      export type GetAllCommentsByPostId = Pick<Helpers.Comment, 'postId'>;
      export type GetAllPostsOfAnFeedUser = void;
    }
    // Response
    namespace Res {
      export type Create = Model.Complete;
      export type UploadPhoto = string;
      export type GetAllPostsOfAnUser = { posts: Model.Complete[]; }
      export type GetPostDetails = { post: Model.Complete; }
      export type AddLike = Model.Complete;
      export type RemoveLike = Model.Complete;
      export type CreateComment = Helpers.Comment;
      export type GetAllCommentsByPostId = Helpers.Comment[];
      export type GetAllPostsOfAnFeedUser = Model.Complete[];
    }
  }

  export namespace Helpers {
    export type Comment = {
      id: string;
      postId: string;
      createdAt: string;
      commentString: string;
      commentUserAvatar: string;
      commentUserUsername: string;
      answeredCommentId?: string;
      edited: boolean;
      usersLikedIdArray: string[];
    }
  }

  export namespace Store {
    export type State = {
      feed: Post.Model.Complete[],
      profileFeed: Post.Model.Complete[],
      detailedPost: Post.Model.Complete
    }
    export enum ActionsName {
      SetFeed = 'SetFeed',
      SetProfileFeed = 'SetProfileFeed',
      SetDetailedPost = 'SetDetailedPost'
    }
    export namespace Actions {
      export type All = SetFeed | SetProfileFeed | SetDetailedPost;
      export type SetFeed = { type: ActionsName.SetFeed; payload: Post.Model.Complete[]; }
      export type SetProfileFeed = { type: ActionsName.SetProfileFeed; payload: Post.Model.Complete[]; }
      export type SetDetailedPost = { type: ActionsName.SetDetailedPost; payload: Post.Model.Complete; }
    }
  }
}