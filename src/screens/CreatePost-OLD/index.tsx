import React from 'react';
import { KeyboardAvoidingView } from 'react-native';
import { AntDesign, Entypo } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';

import { Button, Header, Loading, Space } from '../../components';
import { AddLocation, Photo } from './components';
import { useCreatePost } from './hooks';
import {
  Container, Input, LocationText, ChangePhotoIcon,
  ButtonWrapper, Label, LocationWrapper
} from './styles';

export const CreatePost = () => {
  const {
    isLoading, handleSelectPhoto, photo, handleSubmit, setDescription, description,
    setLocation, step, setStep, location, isLoadingScreen
  } = useCreatePost();

  if (isLoadingScreen) return <Loading />;
  return (
    <Container>
      {step === 'addImage' && (
        <>
          <Header />
          <Space size={500} />
          <Photo photoUri={photo ? photo.uri : undefined} onSelectImage={handleSelectPhoto} />
        </>
      )}

      {step === 'create' && (
        <>
          <Header
            actionFunction={handleSelectPhoto}
            actionIcon={(
              <ChangePhotoIcon>
                <Entypo name="camera" color="white" size={23} />
              </ChangePhotoIcon>
            )}
          />

          <ScrollView>
            <KeyboardAvoidingView behavior="position">
              <Space size={10} />

              <Photo photoUri={photo ? photo.uri : undefined} onSelectImage={handleSelectPhoto} />

              <Space size={20} />

              <Label>Subtitle</Label>
              <Input
                placeholder="Place a caption..."
                placeholderTextColor="#333"
                onChangeText={text => setDescription(text)}
                value={description}
                multiline
              />

              <Space size={20} />

              <LocationWrapper>
                <Label>{location ? 'Change' : 'Add'} location</Label>
                <AntDesign name="pluscircle" size={20} color="black" onPress={() => setStep('addLocation')} />
              </LocationWrapper>
              <LocationText>{location}</LocationText>

              <ButtonWrapper>
                <Button text="POST" color="black" onPress={handleSubmit} isLoading={isLoading} />
              </ButtonWrapper>
            </KeyboardAvoidingView>

            <Space size={150} />
          </ScrollView>
        </>
      )}

      {step === 'addLocation' && (
        <AddLocation setLocation={setLocation} location={location} setStep={setStep} />
      )}
    </Container>
  );
}