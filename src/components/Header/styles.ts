import styled, { css } from 'styled-components/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';
import { Feather } from '@expo/vector-icons';
import { LogoIconLightMode } from '../../assets/icons';

export const Container = styled.View`
  width: 100%;
  background-color: #FFF;
  z-index: 1000;
  margin-bottom: ${RFValue(-20)}px;
  padding-top: ${RFValue(22)}px;
  border-bottom-left-radius: 100px;
  border-bottom-right-radius: 100px;
`;

type WhiteContainerProps = { hasShadow?: boolean; }
export const WhiteContainer = styled.View<WhiteContainerProps>`
  width: 100%;
  height: 70px;
  flex-direction: row;
  background-color: #FFF;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
  justify-content: space-between;
  ${({ hasShadow }) => hasShadow && css`
    shadow-opacity: 0.5;
    shadow-radius: 8px;
    shadow-color: #999;
    shadow-offset: 0px 8px;
  `}
`;

export const BackButton = styled(TouchableOpacity)`
  align-items: center;
  align-content: center;
  margin: auto;
  width: 80px;
`;

export const BackButtonIcon = styled(Feather)`
  font-size: ${RFValue(20)}px;
  font-weight: 800;
  margin-right: auto;
  margin-left: ${RFValue(10)}px;
`;

export const LogoIcon = styled(LogoIconLightMode).attrs({ width: 150 })`
  margin: auto;
`;

export const OptionalButton = styled(TouchableOpacity)`
  width: 80px;
  align-items: center;
  justify-content: center;
  margin: auto;
`;

export const OptionalText = styled.Text`
  color: ${_ => _.theme.colors.primary};
  font-family: ${_ => _.theme.fonts.nunito.bold};
`;