import React from 'react';
import { Keyboard, TouchableWithoutFeedback } from 'react-native';
import { Header, SignInForm } from './components';
import { useSignIn } from './hooks';
import { Container, BikeBackground } from './styles';

export const SignIn: React.FC = () => {
  const {
    control, setRememberMe, rememberMe, errors, goToForgotPasswordScreen, isLoading, handleSignIn
  } = useSignIn();

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Container>
        <Header />

        <SignInForm
          control={control}
          errors={errors}
          goToForgotPasswordScreen={goToForgotPasswordScreen}
          isLoading={isLoading}
          onSubmit={handleSignIn}
        />

        <BikeBackground />
      </Container>
    </TouchableWithoutFeedback>
  );
}