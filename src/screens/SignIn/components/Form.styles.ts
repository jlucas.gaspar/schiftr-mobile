import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Form = styled.View`
  margin-top: ${RFValue(80)}px;
  padding: 20px;
`;

type SpaceProps = { size: number };
export const Space = styled.View<SpaceProps>`
  height: ${_ => _.size}px;
`;

export const Text = styled.Text`
  margin-left: auto;
  color: ${_ => _.theme.colors.text};
  font-family: ${_ => _.theme.fonts.nunito.regular};
  margin-bottom: ${RFValue(10)}px;
`;