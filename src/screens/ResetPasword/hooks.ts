import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { Alert } from 'react-native';
import { useDispatch } from 'react-redux';
import { Auth } from '../../types/Auth';
// import { confirmResetPasswordReceivedToken, updatePassword } from '../../../services/auth';
// import * as userActions from '../../../store/actions/user';
import { updatePasswordResolver } from './resolvers';

export const useConfirmResetPasswordToken = () => {
  const [token, setToken] = useState('');
  const [isLoadingTokenForm, setLoadingTokenForm] = useState(false);
  const [canRenderUpdatePasswordForm, setCanRenderUpdatePasswordForm] = useState(false);

  const handleConfirmResetPasswordToken = async () => {
    // if (token.length !== 6) {
    //   return Alert.alert('The token must have 6 digits.');
    // }

    // const isNumeric = /^\d+$/.test(token);
    // if (!isNumeric) {
    //   return Alert.alert('The token must contain only numeric digits.');
    // }
    
    // setLoadingTokenForm(true);
    // const result = await confirmResetPasswordReceivedToken(token);
    // if (result === `Token is valid.`) {
    //   setCanRenderUpdatePasswordForm(true);
    // }

    // setLoadingTokenForm(false);
  }

  return {
    handleConfirmResetPasswordToken,
    canRenderUpdatePasswordForm,
    isLoadingTokenForm,
    token,
    setToken
  }
}

export const useUpdatePassword = (tokenValue: string) => {
  const [isLoadingPasswordForm, setLoadingPasswordForm] = useState(false);
  const { formState: { errors }, control, handleSubmit } = useForm({ resolver: updatePasswordResolver });
  const dispatch = useDispatch();
  
  const handleUpdatePassword = handleSubmit(async ({ password, passwordConfirmation }: Auth.FormData.UpdatePassword) => {
    // setLoadingPasswordForm(true);
    // const result = await updatePassword({ password, passwordConfirmation, token: tokenValue });
    // if (result) {
    //   dispatch(userActions.authenticate({
    //     user: result.user,
    //     token: result.token
    //   }));
    // }
    // setLoadingPasswordForm(false);
  });

  return {
    passwordErrors: errors,
    passwordControl: control,
    handleUpdatePassword,
    isLoadingPasswordForm
  }
}