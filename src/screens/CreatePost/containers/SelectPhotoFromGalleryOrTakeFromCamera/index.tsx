import React, { useState } from 'react';
import { Platform } from 'react-native';
import * as ImageManipulator from 'expo-image-manipulator';
import * as ImagePicker from 'expo-image-picker';

import { Loading, Header } from '../../../../components';

import { useCreatePostContext } from '../../context';
import { CreatePost } from '../../types';

import { Container, Content, Title, ButtonContainer, ButtonText } from './styles';

export const SelectPhotoFromGalleryOrTakeFromCamera = () => {
  const [isLoading, setLoading] = useState(false);
  const { setPhoto, setStep } = useCreatePostContext();

  const handleSelectPhotoFromGallery = async () => {
    if (Platform.OS !== 'web') {
      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }

    setLoading(true);

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.1
    });

    const { cancelled } = result;
    if (!cancelled) {
      const compressedResult = await ImageManipulator.manipulateAsync(result.uri, [], {
        compress: 0.1
      });
      setPhoto({ ...result, ...compressedResult } as CreatePost.Photo);
      setStep('finishCreate');
    }

    setLoading(false);
  }

  const handleTakePhotoFromCamera = async () => {
    if (Platform.OS !== 'web') {
      const { status } = await ImagePicker.requestCameraPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }

    setLoading(true);

    const result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.1
    });

    const { cancelled } = result;
    if (!cancelled) {
      const compressedResult = await ImageManipulator.manipulateAsync(result.uri, [], {
        compress: 0.1
      });
      setPhoto({ ...result, ...compressedResult } as CreatePost.Photo);
      setStep('finishCreate');
    }

    setLoading(false);
  }

  if (isLoading) return <Loading />;
  return (
    <>
      <Header />

      <Container>
        <Content>
          <Title>Select an image from...</Title>
          <ButtonContainer onPress={handleTakePhotoFromCamera}>
            <ButtonText>Camera</ButtonText>
          </ButtonContainer>
          <ButtonContainer onPress={handleSelectPhotoFromGallery}>
            <ButtonText>Gallery</ButtonText>
          </ButtonContainer>
        </Content>
      </Container>
    </>
  );
}