import styled from 'styled-components/native';
import * as Icon from '../../assets/icons';
import { Feather } from '@expo/vector-icons';
import { RectButton } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';
import { MaterialIcons } from '@expo/vector-icons';

export const Container = styled.View`
  padding: 40px;
  height: 100%;
  width: 100%;
  background-color: #FFF;
`;

export const UserName = styled.Text`
  color: #000;
  font-family: ${_ => _.theme.fonts.raleway.regular};
  font-size: 25px;
  margin: 5px auto;
`;

export const UserCity = styled.Text`
  color: #000;
  font-family: ${_ => _.theme.fonts.raleway.regular};
  font-size: 18px;
  margin: 5px auto;
`;

export const UserPhoto = styled.Image`
  background-color: #555;
  border-radius: 100px;
  height: ${RFValue(120)}px;
  width: ${RFValue(120)}px;
  margin: 0 auto;
`;

export const RoundWhite = styled(RectButton)`
  background-color: #a8a8a8;
  border-radius: ${RFValue(50)}px;
  height: ${RFValue(120)}px;
  width: ${RFValue(120)}px;
  margin: 0 auto;
  justify-content: center;
  align-items: center;
`;

export const EditUserIcon = styled(MaterialIcons)`
  margin: auto;
`;

export const ClickableButtons = styled.View`
  margin-top: ${RFValue(100)}px;
`;

export const ClickableButton = styled(RectButton)`
  color: #e9e9e9;
  flex-direction: row;
  align-items: center;
  margin-bottom: 20px;
`;

export const LockIcon = styled(Feather).attrs({ size: 20 })`
  width: 30px;
  color: #000;
`;

export const ClickableText = styled.Text`
  color: #000;
  font-size: ${RFValue(14)}px;
`;