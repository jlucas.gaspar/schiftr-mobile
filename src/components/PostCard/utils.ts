import * as fns from 'date-fns';

export const parseCreationDateToString = (creationDate: Date) => {
  const now = new Date();
  const creation = new Date(creationDate);

  const hoursDiff = fns.differenceInHours(now, creation);

  if (hoursDiff < 1) {
    const minutesDiff = fns.differenceInMinutes(now, creation);
    if (minutesDiff <= 1) return `${minutesDiff} minute ago`;
    else return `${minutesDiff} minutes ago`;
  }

  if (hoursDiff < 24) {
    if (hoursDiff <= 1) return `${hoursDiff} hour ago`;
    else return `${hoursDiff} hours ago`;
  }

  if (hoursDiff < 168) {
    const daysDiff = fns.differenceInDays(now, creation);
    if (daysDiff <= 1) return `${daysDiff} day ago`;
    else return `${daysDiff} days ago`;
  }

  const weeksDiff = fns.differenceInWeeks(now, creation);
  if (weeksDiff <= 1) return `${weeksDiff} week ago`;
  else return `${weeksDiff} weeks ago`;
}

export const ensureUserLikedPost = (likesArray: string[], userId: string) => {
  const userLiked = likesArray.find(id => id === userId);
  if (userLiked) return true;
  else return false;
}