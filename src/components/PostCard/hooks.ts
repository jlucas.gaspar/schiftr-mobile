import { useState } from 'react';
import { useNavigation } from '@react-navigation/core';
import { useDispatch, useSelector } from 'react-redux';
import { RoutesList } from '../../routes/constants';
import { postService } from '../../services/post';
import { RootState } from '../../store';
import { Post } from '../../types/Post';
import { userWithoutAvatarUrl } from '../../assets/images';
import * as postActions from '../../store/actions/post';

type Input = {
  username: string;
  postId: string;
}

export type PostOptions = 'feed' | 'detailedPost';

export const usePostCard = ({ username, postId }: Input) => {
  const [canComment, setCanComment] = useState(false);
  const [textInputCommentString, setTextInputCommentString] = useState('');
  const { detailedPost, feed } = useSelector((state: RootState) => state.post);
  const { user } = useSelector((state: RootState) => state.user);
  const { navigate } = useNavigation();
  const dispatch = useDispatch();

  const handleGoToProfile = () => {
    navigate(RoutesList.ProfileFeed as never, { username } as never);
  }

  const handleAddLike = async (type: PostOptions) => {
    if (type === 'detailedPost') {
      detailedPost.likes.push(user.id);
      dispatch(postActions.setDetailedPost(detailedPost));
    }

    if (type === 'feed') {
      const newFeedArray: Post.Model.Complete[] = [];
      for (const post of feed) {
        if (post.id === postId) post.likes.push(user.id);
        newFeedArray.push(post)
      }
      dispatch(postActions.setFeed(newFeedArray));
    }

    await postService.addLike({ postId });
  }

  const handleRemoveLike = async (type: PostOptions) => {
    if (type === 'detailedPost') {
      const newLikesArray = [];
      for (const likeId of detailedPost.likes) {
        if (likeId !== user.id) newLikesArray.push(likeId);
      }
      detailedPost.likes = newLikesArray;
      dispatch(postActions.setDetailedPost(detailedPost));
    }

    if (type === 'feed') {
      const newFeedArray: Post.Model.Complete[] = [];

      for (const post of feed) {
        if (post.id === postId) {
          const newLikesArray = [];
          for (const likeId of post.likes) {
            if (likeId !== user.id) newLikesArray.push(likeId);
          }
          post.likes = newLikesArray;
          newFeedArray.push(post);
        } else {
          newFeedArray.push(post);
        }
      }
      dispatch(postActions.setFeed(newFeedArray));
    }

    await postService.removeLike({ postId });
  }

  const handleAddComment = async (type: PostOptions) => {
    setCanComment(false);

    const comment = await postService.createComment({
      commentString: textInputCommentString,
      postId: postId,
      // answeredCommentId: ''
    });

    setTextInputCommentString('');

    if (!comment) {
      return;
    }

    if (type === 'detailedPost') {
      const newComment = {
        commentString: comment.commentString,
        commentUserAvatar: user.avatarImageUrl ? user.avatarImageUrl : userWithoutAvatarUrl,
        commentUserUsername: user.username,
        createdAt: comment.createdAt,
        answeredCommentId: comment.answeredCommentId,
        postId: comment.postId,
        id: comment.id,
        edited: comment.edited,
        usersLikedIdArray: comment.usersLikedIdArray
      }
      detailedPost.comments = [newComment, ...detailedPost.comments];
      detailedPost.commentsNumber = detailedPost.comments.length;

      dispatch(postActions.setDetailedPost(detailedPost));
    }
  }

  const handleEditComment = async (type: PostOptions, newCommentData: Pick<Post.Helpers.Comment, 'id' | 'commentString'>) => {
    if (type === 'detailedPost') {
      const newCommentsArray: Post.Helpers.Comment[] = [];
      for (const comment of detailedPost.comments) {
        if (comment.id !== newCommentData.id) newCommentsArray.push(comment)
        else {
          newCommentsArray.push({
            ...comment,
            commentString: newCommentData.commentString
          });
        }
      }
      detailedPost.comments = newCommentsArray;
      dispatch(postActions.setDetailedPost(detailedPost));
    }

    // await postService.updateComment(...)
  }

  const handleDeleteComment = async (type: PostOptions, commentData: Pick<Post.Helpers.Comment, 'id'>) => {
    if (type === 'detailedPost') {
      const newCommentsArray: Post.Helpers.Comment[] = [];
      for (const comment of detailedPost.comments) {
        if (comment.id !== commentData.id) newCommentsArray.push();
      }
      detailedPost.comments = newCommentsArray;
      dispatch(postActions.setDetailedPost(detailedPost));
    }

    // await postService.removeComment(...)
  }

  return {
    canComment,
    handleGoToProfile,
    // handleGoToPostDetails,
    setCanComment,
    handleAddLike,
    handleRemoveLike,
    handleAddComment,
    handleEditComment,
    handleDeleteComment,
    setTextInputCommentString,
    textInputCommentString
  }
}