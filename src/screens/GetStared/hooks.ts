import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { RoutesList } from '../../routes/constants';
import { authService } from '../../services/auth';
import * as userActions from '../../store/actions/user';

export const useGetStarted = () => {
  const [isLoading, setLoading] = useState(false);
  const { navigate } = useNavigation();
  const dispatch = useDispatch();

  const goToSignUpScreen = () => navigate(RoutesList.SignUp as any);
  const goToSignInScreen = () => navigate(RoutesList.SignIn as any);

  const handleGoogleSignUp = async () => {
    setLoading(true);

    const response = await authService.googleSignUp();

    if (!response) {
      setLoading(false);
      return;
    }

    dispatch(userActions.authenticate({
      user: response.user,
      token: response.token
    }));

    setLoading(false);
  }

  return { isLoading, handleGoogleSignUp, goToSignUpScreen, goToSignInScreen }
}