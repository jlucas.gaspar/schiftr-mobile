import { BottomTabNavigationOptions } from '@react-navigation/bottom-tabs';
import theme from '../../../styles/ThemeProvider/theme';

type Theme = typeof theme;

export const makeTabBarNavigatorScreenOptions = (theme: Theme): BottomTabNavigationOptions => ({
  headerShown: false,
  tabBarShowLabel: false,
  tabBarActiveTintColor: theme.colors.primary,
  tabBarInactiveTintColor: '#555',
  tabBarStyle: {
    marginBottom: 20,
    marginHorizontal: 30, //10,
    borderRadius: 20,
    position: 'absolute',
    paddingBottom: -40,
    height: 50,
    shadowOpacity: 1,
    shadowRadius: 5,
    shadowColor: '#333',
    shadowOffset: { height: 10, width: 0 },
    elevation: 5
  }
});

export const tabBarHidden: BottomTabNavigationOptions = {
  tabBarItemStyle: {
    display: 'none'
  }
}