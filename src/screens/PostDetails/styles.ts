import styled from 'styled-components/native';
import { getBottomSpace } from 'react-native-iphone-x-helper';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.ScrollView`
  margin-bottom: ${getBottomSpace()}px;
`;

export const Divider = styled.View`
  margin: ${RFValue(10)}px ${RFValue(5)}px;
  height: 2px;
  background-color: #C9C9C9;
`;