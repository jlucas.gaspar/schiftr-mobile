import React, { createContext, useContext, useState } from 'react';
import { WithChildren } from '../../types/helpers';
import { CreatePost } from './types';

const Ctx = createContext<CreatePost.ContextState>({} as CreatePost.ContextState);

export const CreatePostContextProvider = ({ children }: WithChildren) => {
  const [photo, setPhoto] = useState<CreatePost.Photo>();
  const [location, setLocation] = useState<string>();
  const [step, setStep] = useState<CreatePost.Step>('selectPhotoFromGalleryOrTakeFromCamera');

  return (
    <Ctx.Provider value={{ photo, setPhoto, step, setStep, location, setLocation }}>
      {children}
    </Ctx.Provider>
  );
}

export const useCreatePostContext = () => {
  const context = useContext(Ctx);
  if (!context) throw new Error('"useCreatePost()" must be wrapped inside <CreatePostContextProvider /> component.');
  return context;
}