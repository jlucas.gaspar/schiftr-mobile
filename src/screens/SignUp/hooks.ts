import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Auth } from '../../types/Auth';
import { authService } from '../../services/auth';
import * as userActions from '../../store/actions/user';
import { signUpSchema } from './schema';

const resolver = yupResolver(signUpSchema);

type FormData = Auth.FormData.EmailSignUp;

export const useSignUp = () => {
  const [agree, setAgree] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [isLoadingGoogleAuth, setLoadingGoogleAuth] = useState(false);
  const { formState: { errors }, handleSubmit, control } = useForm({ resolver });
  const dispatch = useDispatch();

  const handleSignUp = handleSubmit(async ({ email, username, password, passwordConfirmation }: FormData) => {
    setLoading(true);
    const result = await authService.emailSignUp({ passwordConfirmation, username, password, email });

    if (!result) {
      return setLoading(false);
    }

    dispatch(userActions.authenticate({
      user: result.user,
      token: result.token
    }));

    setLoading(false);
  });

  const handleGoogleSignUp = async () => {
    setLoadingGoogleAuth(true);
    const result = await authService.googleSignUp();

    if (!result) {
      setLoading(false);
      return;
    }

    dispatch(userActions.authenticate(result));
    setLoading(false);
  }

  return { isLoading, isLoadingGoogleAuth, errors, handleSignUp, control, agree, setAgree, handleGoogleSignUp, setLoading }
}