import { Follow } from '../types/Follow';
import { makeApiRequest } from './helpers/axios';
import { handleServiceError } from './helpers/errorHandler';

type FollowService = {
  follow: (input: Follow.Http.Request.Follow) => Promise<Follow.Http.Response.Follow>;
  unfollow: (input: Follow.Http.Request.Unfollow) => Promise<Follow.Http.Response.Unfollow>;
  ensureIsFollowing: (input: Follow.Http.Request.EnsureIsFollowing) => Promise<Follow.Http.Response.EnsureIsFollowing>;
  getNumberOfFollowing: (input: Follow.Http.Request.GetNumberOfFollowing) => Promise<Follow.Http.Response.GetNumberOfFollowing>;
  getNumberOfFollowers: (input: Follow.Http.Request.GetNumberOfFollowers) => Promise<Follow.Http.Response.GetNumberOfFollowers>;
}

export const followService: FollowService = {
  follow: async ({ userFollowedId }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: '/follow',
        data: { userFollowedId }
      });
    } catch (error) {
      handleServiceError(error, 'follow');
    }
  },

  unfollow: async ({ userFollowedId }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: '/unfollow',
        data: { userFollowedId }
      });
    } catch (error) {
      handleServiceError(error, 'getAllCommentsByPostId');
    }
  },

  ensureIsFollowing: async ({ userFollowedId }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/follow-relation/${userFollowedId}`
      });
    } catch (error) {
      handleServiceError(error, 'ensureIsFollowing');
    }
  },

  getNumberOfFollowers: async ({ userId }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/number-followers/${userId}`
      });
    } catch (error) {
      handleServiceError(error, 'ensureIsFollowing');
    }
  },

  getNumberOfFollowing: async ({ userId }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/number-following/${userId}`
      });
    } catch (error) {
      handleServiceError(error, 'ensureIsFollowing');
    }
  }
}