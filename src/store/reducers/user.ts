import { User } from '../../types/User';

type Actions = User.Store.Actions.All;
type State = User.Store.State;

const initialState: State = {
  user: {} as User.Model.Complete,
  isAuthenticated: false,
  token: {
    token: '',
    exp: 0
  }
}

export const userReducer = (state = initialState, action: Actions): State => {
  switch (action.type) {
    case User.Store.ActionsName.Authenticate:
      return { ...state, isAuthenticated: true, user: action.payload.user, token: { token: action.payload.token.token, exp: action.payload.token.exp } };

    case User.Store.ActionsName.Logout:
      return { ...state, user: {} as User.Model.Complete, token: { token: '', exp: 0 }, isAuthenticated: false };

    case User.Store.ActionsName.SetUser:
      return { ...state, user: { ...state.user, ...action.payload } }

    default:
      return state;
  }
}