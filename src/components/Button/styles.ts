import styled, { css } from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';

type ButtonProps = { isLoading?: boolean; color: 'black' | 'white'; }
export const StyledButton = styled(RectButton) <ButtonProps>`
  width: 100%;
  ${({ color, isLoading }) => color === 'black' && css`
    background-color: ${({ theme }) => isLoading ? '#333' : theme.colors.background};
  `}
  ${({ color, isLoading }) => color === 'white' && css`
    background-color: ${({ theme }) => isLoading ? '#c9c9c9' : theme.colors.text};
  `}
  border-radius: 20px;
  margin: 5px 0;
  padding: 10px;
  flex-direction: row;
`;

export const Text = styled.Text<ButtonProps>`
  margin: auto;
  font-family: ${({ theme }) => theme.fonts.nunito.medium};
  font-size: ${RFValue(15)}px;
  ${({ color }) => color === 'black' && css`color: #FFF;`}
  ${({ color }) => color === 'white' && css`color: #000;`}
`;

export const IconWrapper = styled.View`
  margin-left: 15px;
  margin-right: -15px;
`;