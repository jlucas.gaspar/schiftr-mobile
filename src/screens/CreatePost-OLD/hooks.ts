import * as ImageManipulator from 'expo-image-manipulator';
import * as ImagePicker from 'expo-image-picker';
import { useState, useEffect } from 'react';
import { Alert, Platform } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { RoutesList } from '../../routes/constants';
import { postService } from '../../services/post';

type Photo = {
  uri: string;
  width: number;
  height: number;
  type?: 'image';
  base64?: string;
  exif?: {
    [key: string]: any;
  };
}

export type Step = 'addLocation' | 'create' | 'addImage';

export const useCreatePost = () => {
  const [photo, setPhoto] = useState<Photo | null>(null);
  const [description, setDescription] = useState<string | undefined>(undefined);
  const [isLoading, setLoading] = useState(false);
  const [isLoadingScreen, setLoadingScreen] = useState(false);
  const [location, setLocation] = useState<string | undefined>(undefined);
  const [step, setStep] = useState<Step>('addImage');
  const { navigate } = useNavigation();

  useEffect(() => {
    if (step === 'addImage') {
      handleSelectPhoto();
    }
  }, [step]);

  const handleSelectPhoto = async () => {
    if (Platform.OS !== 'web') {
      const result = await ImagePicker.requestCameraPermissionsAsync();

      const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }

    setLoadingScreen(true);

    const result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 0.1
    });

    const { cancelled } = result;

    if (cancelled) {
      setLoadingScreen(false);
      return;
    }

    const compressedResult = await ImageManipulator.manipulateAsync(result.uri, [], {
      compress: 0.1
    });

    setPhoto({ ...result, ...compressedResult } as Photo);
    setStep('create');
    setLoadingScreen(false);
  }

  const handleSubmit = async () => {
    if (!photo?.uri) {
      Alert.alert('You must select a photo before creating a post.');
      return;
    }

    setLoading(true);

    const fileUrl = await postService.uploadPhoto({ fileUri: photo.uri });

    if (!fileUrl) {
      return setLoading(false);
    }

    await postService.createPost({
      location: location,
      text: description,
      imagesUrl: [fileUrl],
      isPrivate: false
    });

    setLoading(false);
    navigate(RoutesList.Feed as any);
  };

  return {
    isLoading,
    handleSelectPhoto,
    photo, 
    handleSubmit,
    setDescription,
    description,
    setLocation,
    location,
    setStep,
    step,
    isLoadingScreen
  }
}