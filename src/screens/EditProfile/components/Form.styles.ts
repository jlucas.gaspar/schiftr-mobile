import styled from 'styled-components/native';
import { Input } from '../../../components';

export const Form = styled.View``;

export const TextInput = styled(Input)`
  border-bottom-color: #FFF;
`;