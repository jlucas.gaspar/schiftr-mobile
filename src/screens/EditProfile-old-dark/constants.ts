export const genres = [
  { label: 'M', value: 'M', key: 'M' },
  { label: 'F', value: 'F', key: 'F' }
];

export const countries = [{
  key: "AF_Afghanistan",
  label: "Afghanistan",
  value: "AF_Afghanistan"
}, {
  key: "AX_Åland Islands",
  label: "Åland Islands",
  value: "AX_Åland Islands"
}, {
  key: "AL_Albania",
  label: "Albania",
  value: "AL_Albania"
}, {
  key: "DZ_Algeria",
  label: "Algeria",
  value: "DZ_Algeria"
}, {
  key: "AS_American Samoa",
  label: "American Samoa",
  value: "AS_American Samoa"
}, {
  key: "AD_AndorrA",
  label: "AndorrA",
  value: "AD_AndorrA"
}, {
  key: "AO_Angola",
  label: "Angola",
  value: "AO_Angola"
}, {
  key: "AI_Anguilla",
  label: "Anguilla",
  value: "AI_Anguilla"
}, {
  key: "AQ_Antarctica",
  label: "Antarctica",
  value: "AQ_Antarctica"
}, {
  key: "AG_Antigua and Barbuda",
  label: "Antigua and Barbuda",
  value: "AG_Antigua and Barbuda"
}, {
  key: "AR_Argentina",
  label: "Argentina",
  value: "AR_Argentina"
}, {
  key: "AM_Armenia",
  label: "Armenia",
  value: "AM_Armenia"
}, {
  key: "AW_Aruba",
  label: "Aruba",
  value: "AW_Aruba"
}, {
  key: "AU_Australia",
  label: "Australia",
  value: "AU_Australia"
}, {
  key: "AT_Austria",
  label: "Austria",
  value: "AT_Austria"
}, {
  key: "AZ_Azerbaijan",
  label: "Azerbaijan",
  value: "AZ_Azerbaijan"
}, {
  key: "BS_Bahamas",
  label: "Bahamas",
  value: "BS_Bahamas"
}, {
  key: "BH_Bahrain",
  label: "Bahrain",
  value: "BH_Bahrain"
}, {
  key: "BD_Bangladesh",
  label: "Bangladesh",
  value: "BD_Bangladesh"
}, {
  key: "BB_Barbados",
  label: "Barbados",
  value: "BB_Barbados"
}, {
  key: "BY_Belarus",
  label: "Belarus",
  value: "BY_Belarus"
}, {
  key: "BE_Belgium",
  label: "Belgium",
  value: "BE_Belgium"
}, {
  key: "BZ_Belize",
  label: "Belize",
  value: "BZ_Belize"
}, {
  key: "BJ_Benin",
  label: "Benin",
  value: "BJ_Benin"
}, {
  key: "BM_Bermuda",
  label: "Bermuda",
  value: "BM_Bermuda"
}, {
  key: "BT_Bhutan",
  label: "Bhutan",
  value: "BT_Bhutan"
}, {
  key: "BO_Bolivia",
  label: "Bolivia",
  value: "BO_Bolivia"
}, {
  key: "BA_Bosnia and Herzegovina",
  label: "Bosnia and Herzegovina",
  value: "BA_Bosnia and Herzegovina"
}, {
  key: "BW_Botswana",
  label: "Botswana",
  value: "BW_Botswana"
}, {
  key: "BV_Bouvet Island",
  label: "Bouvet Island",
  value: "BV_Bouvet Island"
}, {
  key: "BR_Brazil",
  label: "Brazil",
  value: "BR_Brazil"
}, {
  key: "IO_British Indian Ocean Territory",
  label: "British Indian Ocean Territory",
  value: "IO_British Indian Ocean Territory"
}, {
  key: "BN_Brunei Darussalam",
  label: "Brunei Darussalam",
  value: "BN_Brunei Darussalam"
}, {
  key: "BG_Bulgaria",
  label: "Bulgaria",
  value: "BG_Bulgaria"
}, {
  key: "BF_Burkina Faso",
  label: "Burkina Faso",
  value: "BF_Burkina Faso"
}, {
  key: "BI_Burundi",
  label: "Burundi",
  value: "BI_Burundi"
}, {
  key: "KH_Cambodia",
  label: "Cambodia",
  value: "KH_Cambodia"
}, {
  key: "CM_Cameroon",
  label: "Cameroon",
  value: "CM_Cameroon"
}, {
  key: "CA_Canada",
  label: "Canada",
  value: "CA_Canada"
}, {
  key: "CV_Cape Verde",
  label: "Cape Verde",
  value: "CV_Cape Verde"
}, {
  key: "KY_Cayman Islands",
  label: "Cayman Islands",
  value: "KY_Cayman Islands"
}, {
  key: "CF_Central African Republic",
  label: "Central African Republic",
  value: "CF_Central African Republic"
}, {
  key: "TD_Chad",
  label: "Chad",
  value: "TD_Chad"
}, {
  key: "CL_Chile",
  label: "Chile",
  value: "CL_Chile"
}, {
  key: "CN_China",
  label: "China",
  value: "CN_China"
}, {
  key: "CX_Christmas Island",
  label: "Christmas Island",
  value: "CX_Christmas Island"
}, {
  key: "CC_Cocos (Keeling) Islands",
  label: "Cocos (Keeling) Islands",
  value: "CC_Cocos (Keeling) Islands"
}, {
  key: "CO_Colombia",
  label: "Colombia",
  value: "CO_Colombia"
}, {
  key: "KM_Comoros",
  label: "Comoros",
  value: "KM_Comoros"
}, {
  key: "CG_Congo",
  label: "Congo",
  value: "CG_Congo"
}, {
  key: "CD_Congo, The Democratic Republic of the",
  label: "Congo, The Democratic Republic of the",
  value: "CD_Congo, The Democratic Republic of the"
}, {
  key: "CK_Cook Islands",
  label: "Cook Islands",
  value: "CK_Cook Islands"
}, {
  key: "CR_Costa Rica",
  label: "Costa Rica",
  value: "CR_Costa Rica"
}, {
  key: "CI_Cote D'Ivoire",
  label: "Cote D'Ivoire",
  value: "CI_Cote D'Ivoire"
}, {
  key: "HR_Croatia",
  label: "Croatia",
  value: "HR_Croatia"
}, {
  key: "CU_Cuba",
  label: "Cuba",
  value: "CU_Cuba"
}, {
  key: "CY_Cyprus",
  label: "Cyprus",
  value: "CY_Cyprus"
}, {
  key: "CZ_Czech Republic",
  label: "Czech Republic",
  value: "CZ_Czech Republic"
}, {
  key: "DK_Denmark",
  label: "Denmark",
  value: "DK_Denmark"
}, {
  key: "DJ_Djibouti",
  label: "Djibouti",
  value: "DJ_Djibouti"
}, {
  key: "DM_Dominica",
  label: "Dominica",
  value: "DM_Dominica"
}, {
  key: "DO_Dominican Republic",
  label: "Dominican Republic",
  value: "DO_Dominican Republic"
}, {
  key: "EC_Ecuador",
  label: "Ecuador",
  value: "EC_Ecuador"
}, {
  key: "EG_Egypt",
  label: "Egypt",
  value: "EG_Egypt"
}, {
  key: "SV_El Salvador",
  label: "El Salvador",
  value: "SV_El Salvador"
}, {
  key: "GQ_Equatorial Guinea",
  label: "Equatorial Guinea",
  value: "GQ_Equatorial Guinea"
}, {
  key: "ER_Eritrea",
  label: "Eritrea",
  value: "ER_Eritrea"
}, {
  key: "EE_Estonia",
  label: "Estonia",
  value: "EE_Estonia"
}, {
  key: "ET_Ethiopia",
  label: "Ethiopia",
  value: "ET_Ethiopia"
}, {
  key: "FK_Falkland Islands (Malvinas)",
  label: "Falkland Islands (Malvinas)",
  value: "FK_Falkland Islands (Malvinas)"
}, {
  key: "FO_Faroe Islands",
  label: "Faroe Islands",
  value: "FO_Faroe Islands"
}, {
  key: "FJ_Fiji",
  label: "Fiji",
  value: "FJ_Fiji"
}, {
  key: "FI_Finland",
  label: "Finland",
  value: "FI_Finland"
}, {
  key: "FR_France",
  label: "France",
  value: "FR_France"
}, {
  key: "GF_French Guiana",
  label: "French Guiana",
  value: "GF_French Guiana"
}, {
  key: "PF_French Polynesia",
  label: "French Polynesia",
  value: "PF_French Polynesia"
}, {
  key: "TF_French Southern Territories",
  label: "French Southern Territories",
  value: "TF_French Southern Territories"
}, {
  key: "GA_Gabon",
  label: "Gabon",
  value: "GA_Gabon"
}, {
  key: "GM_Gambia",
  label: "Gambia",
  value: "GM_Gambia"
}, {
  key: "GE_Georgia",
  label: "Georgia",
  value: "GE_Georgia"
}, {
  key: "DE_Germany",
  label: "Germany",
  value: "DE_Germany"
}, {
  key: "GH_Ghana",
  label: "Ghana",
  value: "GH_Ghana"
}, {
  key: "GI_Gibraltar",
  label: "Gibraltar",
  value: "GI_Gibraltar"
}, {
  key: "GR_Greece",
  label: "Greece",
  value: "GR_Greece"
}, {
  key: "GL_Greenland",
  label: "Greenland",
  value: "GL_Greenland"
}, {
  key: "GD_Grenada",
  label: "Grenada",
  value: "GD_Grenada"
}, {
  key: "GP_Guadeloupe",
  label: "Guadeloupe",
  value: "GP_Guadeloupe"
}, {
  key: "GU_Guam",
  label: "Guam",
  value: "GU_Guam"
}, {
  key: "GT_Guatemala",
  label: "Guatemala",
  value: "GT_Guatemala"
}, {
  key: "GG_Guernsey",
  label: "Guernsey",
  value: "GG_Guernsey"
}, {
  key: "GN_Guinea",
  label: "Guinea",
  value: "GN_Guinea"
}, {
  key: "GW_Guinea-Bissau",
  label: "Guinea-Bissau",
  value: "GW_Guinea-Bissau"
}, {
  key: "GY_Guyana",
  label: "Guyana",
  value: "GY_Guyana"
}, {
  key: "HT_Haiti",
  label: "Haiti",
  value: "HT_Haiti"
}, {
  key: "HM_Heard Island and Mcdonald Islands",
  label: "Heard Island and Mcdonald Islands",
  value: "HM_Heard Island and Mcdonald Islands"
}, {
  key: "VA_Holy See (Vatican City State)",
  label: "Holy See (Vatican City State)",
  value: "VA_Holy See (Vatican City State)"
}, {
  key: "HN_Honduras",
  label: "Honduras",
  value: "HN_Honduras"
}, {
  key: "HK_Hong Kong",
  label: "Hong Kong",
  value: "HK_Hong Kong"
}, {
  key: "HU_Hungary",
  label: "Hungary",
  value: "HU_Hungary"
}, {
  key: "IS_Iceland",
  label: "Iceland",
  value: "IS_Iceland"
}, {
  key: "IN_India",
  label: "India",
  value: "IN_India"
}, {
  key: "ID_Indonesia",
  label: "Indonesia",
  value: "ID_Indonesia"
}, {
  key: "IR_Iran, Islamic Republic Of",
  label: "Iran, Islamic Republic Of",
  value: "IR_Iran, Islamic Republic Of"
}, {
  key: "IQ_Iraq",
  label: "Iraq",
  value: "IQ_Iraq"
}, {
  key: "IE_Ireland",
  label: "Ireland",
  value: "IE_Ireland"
}, {
  key: "IM_Isle of Man",
  label: "Isle of Man",
  value: "IM_Isle of Man"
}, {
  key: "IL_Israel",
  label: "Israel",
  value: "IL_Israel"
}, {
  key: "IT_Italy",
  label: "Italy",
  value: "IT_Italy"
}, {
  key: "JM_Jamaica",
  label: "Jamaica",
  value: "JM_Jamaica"
}, {
  key: "JP_Japan",
  label: "Japan",
  value: "JP_Japan"
}, {
  key: "JE_Jersey",
  label: "Jersey",
  value: "JE_Jersey"
}, {
  key: "JO_Jordan",
  label: "Jordan",
  value: "JO_Jordan"
}, {
  key: "KZ_Kazakhstan",
  label: "Kazakhstan",
  value: "KZ_Kazakhstan"
}, {
  key: "KE_Kenya",
  label: "Kenya",
  value: "KE_Kenya"
}, {
  key: "KI_Kiribati",
  label: "Kiribati",
  value: "KI_Kiribati"
}, {
  key: "KP_Korea, Democratic People'S Republic of",
  label: "Korea, Democratic People'S Republic of",
  value: "KP_Korea, Democratic People'S Republic of"
}, {
  key: "KR_Korea, Republic of",
  label: "Korea, Republic of",
  value: "KR_Korea, Republic of"
}, {
  key: "KW_Kuwait",
  label: "Kuwait",
  value: "KW_Kuwait"
}, {
  key: "KG_Kyrgyzstan",
  label: "Kyrgyzstan",
  value: "KG_Kyrgyzstan"
}, {
  key: "LA_Lao People'S Democratic Republic",
  label: "Lao People'S Democratic Republic",
  value: "LA_Lao People'S Democratic Republic"
}, {
  key: "LV_Latvia",
  label: "Latvia",
  value: "LV_Latvia"
}, {
  key: "LB_Lebanon",
  label: "Lebanon",
  value: "LB_Lebanon"
}, {
  key: "LS_Lesotho",
  label: "Lesotho",
  value: "LS_Lesotho"
}, {
  key: "LR_Liberia",
  label: "Liberia",
  value: "LR_Liberia"
}, {
  key: "LY_Libyan Arab Jamahiriya",
  label: "Libyan Arab Jamahiriya",
  value: "LY_Libyan Arab Jamahiriya"
}, {
  key: "LI_Liechtenstein",
  label: "Liechtenstein",
  value: "LI_Liechtenstein"
}, {
  key: "LT_Lithuania",
  label: "Lithuania",
  value: "LT_Lithuania"
}, {
  key: "LU_Luxembourg",
  label: "Luxembourg",
  value: "LU_Luxembourg"
}, {
  key: "MO_Macao",
  label: "Macao",
  value: "MO_Macao"
}, {
  key: "MK_Macedonia, The Former Yugoslav Republic of",
  label: "Macedonia, The Former Yugoslav Republic of",
  value: "MK_Macedonia, The Former Yugoslav Republic of"
}, {
  key: "MG_Madagascar",
  label: "Madagascar",
  value: "MG_Madagascar"
}, {
  key: "MW_Malawi",
  label: "Malawi",
  value: "MW_Malawi"
}, {
  key: "MY_Malaysia",
  label: "Malaysia",
  value: "MY_Malaysia"
}, {
  key: "MV_Maldives",
  label: "Maldives",
  value: "MV_Maldives"
}, {
  key: "ML_Mali",
  label: "Mali",
  value: "ML_Mali"
}, {
  key: "MT_Malta",
  label: "Malta",
  value: "MT_Malta"
}, {
  key: "MH_Marshall Islands",
  label: "Marshall Islands",
  value: "MH_Marshall Islands"
}, {
  key: "MQ_Martinique",
  label: "Martinique",
  value: "MQ_Martinique"
}, {
  key: "MR_Mauritania",
  label: "Mauritania",
  value: "MR_Mauritania"
}, {
  key: "MU_Mauritius",
  label: "Mauritius",
  value: "MU_Mauritius"
}, {
  key: "YT_Mayotte",
  label: "Mayotte",
  value: "YT_Mayotte"
}, {
  key: "MX_Mexico",
  label: "Mexico",
  value: "MX_Mexico"
}, {
  key: "FM_Micronesia, Federated States of",
  label: "Micronesia, Federated States of",
  value: "FM_Micronesia, Federated States of"
}, {
  key: "MD_Moldova, Republic of",
  label: "Moldova, Republic of",
  value: "MD_Moldova, Republic of"
}, {
  key: "MC_Monaco",
  label: "Monaco",
  value: "MC_Monaco"
}, {
  key: "MN_Mongolia",
  label: "Mongolia",
  value: "MN_Mongolia"
}, {
  key: "MS_Montserrat",
  label: "Montserrat",
  value: "MS_Montserrat"
}, {
  key: "MA_Morocco",
  label: "Morocco",
  value: "MA_Morocco"
}, {
  key: "MZ_Mozambique",
  label: "Mozambique",
  value: "MZ_Mozambique"
}, {
  key: "MM_Myanmar",
  label: "Myanmar",
  value: "MM_Myanmar"
}, {
  key: "NA_Namibia",
  label: "Namibia",
  value: "NA_Namibia"
}, {
  key: "NR_Nauru",
  label: "Nauru",
  value: "NR_Nauru"
}, {
  key: "NP_Nepal",
  label: "Nepal",
  value: "NP_Nepal"
}, {
  key: "NL_Netherlands",
  label: "Netherlands",
  value: "NL_Netherlands"
}, {
  key: "AN_Netherlands Antilles",
  label: "Netherlands Antilles",
  value: "AN_Netherlands Antilles"
}, {
  key: "NC_New Caledonia",
  label: "New Caledonia",
  value: "NC_New Caledonia"
}, {
  key: "NZ_New Zealand",
  label: "New Zealand",
  value: "NZ_New Zealand"
}, {
  key: "NI_Nicaragua",
  label: "Nicaragua",
  value: "NI_Nicaragua"
}, {
  key: "NE_Niger",
  label: "Niger",
  value: "NE_Niger"
}, {
  key: "NG_Nigeria",
  label: "Nigeria",
  value: "NG_Nigeria"
}, {
  key: "NU_Niue",
  label: "Niue",
  value: "NU_Niue"
}, {
  key: "NF_Norfolk Island",
  label: "Norfolk Island",
  value: "NF_Norfolk Island"
}, {
  key: "MP_Northern Mariana Islands",
  label: "Northern Mariana Islands",
  value: "MP_Northern Mariana Islands"
}, {
  key: "NO_Norway",
  label: "Norway",
  value: "NO_Norway"
}, {
  key: "OM_Oman",
  label: "Oman",
  value: "OM_Oman"
}, {
  key: "PK_Pakistan",
  label: "Pakistan",
  value: "PK_Pakistan"
}, {
  key: "PW_Palau",
  label: "Palau",
  value: "PW_Palau"
}, {
  key: "PS_Palestinian Territory, Occupied",
  label: "Palestinian Territory, Occupied",
  value: "PS_Palestinian Territory, Occupied"
}, {
  key: "PA_Panama",
  label: "Panama",
  value: "PA_Panama"
}, {
  key: "PG_Papua New Guinea",
  label: "Papua New Guinea",
  value: "PG_Papua New Guinea"
}, {
  key: "PY_Paraguay",
  label: "Paraguay",
  value: "PY_Paraguay"
}, {
  key: "PE_Peru",
  label: "Peru",
  value: "PE_Peru"
}, {
  key: "PH_Philippines",
  label: "Philippines",
  value: "PH_Philippines"
}, {
  key: "PN_Pitcairn",
  label: "Pitcairn",
  value: "PN_Pitcairn"
}, {
  key: "PL_Poland",
  label: "Poland",
  value: "PL_Poland"
}, {
  key: "PT_Portugal",
  label: "Portugal",
  value: "PT_Portugal"
}, {
  key: "PR_Puerto Rico",
  label: "Puerto Rico",
  value: "PR_Puerto Rico"
}, {
  key: "QA_Qatar",
  label: "Qatar",
  value: "QA_Qatar"
}, {
  key: "RE_Reunion",
  label: "Reunion",
  value: "RE_Reunion"
}, {
  key: "RO_Romania",
  label: "Romania",
  value: "RO_Romania"
}, {
  key: "RU_Russian Federation",
  label: "Russian Federation",
  value: "RU_Russian Federation"
}, {
  key: "RW_RWANDA",
  label: "RWANDA",
  value: "RW_RWANDA"
}, {
  key: "SH_Saint Helena",
  label: "Saint Helena",
  value: "SH_Saint Helena"
}, {
  key: "KN_Saint Kitts and Nevis",
  label: "Saint Kitts and Nevis",
  value: "KN_Saint Kitts and Nevis"
}, {
  key: "LC_Saint Lucia",
  label: "Saint Lucia",
  value: "LC_Saint Lucia"
}, {
  key: "PM_Saint Pierre and Miquelon",
  label: "Saint Pierre and Miquelon",
  value: "PM_Saint Pierre and Miquelon"
}, {
  key: "VC_Saint Vincent and the Grenadines",
  label: "Saint Vincent and the Grenadines",
  value: "VC_Saint Vincent and the Grenadines"
}, {
  key: "WS_Samoa",
  label: "Samoa",
  value: "WS_Samoa"
}, {
  key: "SM_San Marino",
  label: "San Marino",
  value: "SM_San Marino"
}, {
  key: "ST_Sao Tome and Principe",
  label: "Sao Tome and Principe",
  value: "ST_Sao Tome and Principe"
}, {
  key: "SA_Saudi Arabia",
  label: "Saudi Arabia",
  value: "SA_Saudi Arabia"
}, {
  key: "SN_Senegal",
  label: "Senegal",
  value: "SN_Senegal"
}, {
  key: "CS_Serbia and Montenegro",
  label: "Serbia and Montenegro",
  value: "CS_Serbia and Montenegro"
}, {
  key: "SC_Seychelles",
  label: "Seychelles",
  value: "SC_Seychelles"
}, {
  key: "SL_Sierra Leone",
  label: "Sierra Leone",
  value: "SL_Sierra Leone"
}, {
  key: "SG_Singapore",
  label: "Singapore",
  value: "SG_Singapore"
}, {
  key: "SK_Slovakia",
  label: "Slovakia",
  value: "SK_Slovakia"
}, {
  key: "SI_Slovenia",
  label: "Slovenia",
  value: "SI_Slovenia"
}, {
  key: "SB_Solomon Islands",
  label: "Solomon Islands",
  value: "SB_Solomon Islands"
}, {
  key: "SO_Somalia",
  label: "Somalia",
  value: "SO_Somalia"
}, {
  key: "ZA_South Africa",
  label: "South Africa",
  value: "ZA_South Africa"
}, {
  key: "GS_South Georgia and the South Sandwich Islands",
  label: "South Georgia and the South Sandwich Islands",
  value: "GS_South Georgia and the South Sandwich Islands"
}, {
  key: "ES_Spain",
  label: "Spain",
  value: "ES_Spain"
}, {
  key: "LK_Sri Lanka",
  label: "Sri Lanka",
  value: "LK_Sri Lanka"
}, {
  key: "SD_Sudan",
  label: "Sudan",
  value: "SD_Sudan"
}, {
  key: "SR_Suriname",
  label: "Suriname",
  value: "SR_Suriname"
}, {
  key: "SJ_Svalbard and Jan Mayen",
  label: "Svalbard and Jan Mayen",
  value: "SJ_Svalbard and Jan Mayen"
}, {
  key: "SZ_Swaziland",
  label: "Swaziland",
  value: "SZ_Swaziland"
}, {
  key: "SE_Sweden",
  label: "Sweden",
  value: "SE_Sweden"
}, {
  key: "CH_Switzerland",
  label: "Switzerland",
  value: "CH_Switzerland"
}, {
  key: "SY_Syrian Arab Republic",
  label: "Syrian Arab Republic",
  value: "SY_Syrian Arab Republic"
}, {
  key: "TW_Taiwan, Province of China",
  label: "Taiwan, Province of China",
  value: "TW_Taiwan, Province of China"
}, {
  key: "TJ_Tajikistan",
  label: "Tajikistan",
  value: "TJ_Tajikistan"
}, {
  key: "TZ_Tanzania, United Republic of",
  label: "Tanzania, United Republic of",
  value: "TZ_Tanzania, United Republic of"
}, {
  key: "TH_Thailand",
  label: "Thailand",
  value: "TH_Thailand"
}, {
  key: "TL_Timor-Leste",
  label: "Timor-Leste",
  value: "TL_Timor-Leste"
}, {
  key: "TG_Togo",
  label: "Togo",
  value: "TG_Togo"
}, {
  key: "TK_Tokelau",
  label: "Tokelau",
  value: "TK_Tokelau"
}, {
  key: "TO_Tonga",
  label: "Tonga",
  value: "TO_Tonga"
}, {
  key: "TT_Trinidad and Tobago",
  label: "Trinidad and Tobago",
  value: "TT_Trinidad and Tobago"
}, {
  key: "TN_Tunisia",
  label: "Tunisia",
  value: "TN_Tunisia"
}, {
  key: "TR_Turkey",
  label: "Turkey",
  value: "TR_Turkey"
}, {
  key: "TM_Turkmenistan",
  label: "Turkmenistan",
  value: "TM_Turkmenistan"
}, {
  key: "TC_Turks and Caicos Islands",
  label: "Turks and Caicos Islands",
  value: "TC_Turks and Caicos Islands"
}, {
  key: "TV_Tuvalu",
  label: "Tuvalu",
  value: "TV_Tuvalu"
}, {
  key: "UG_Uganda",
  label: "Uganda",
  value: "UG_Uganda"
}, {
  key: "UA_Ukraine",
  label: "Ukraine",
  value: "UA_Ukraine"
}, {
  key: "AE_United Arab Emirates",
  label: "United Arab Emirates",
  value: "AE_United Arab Emirates"
}, {
  key: "GB_United Kingdom",
  label: "United Kingdom",
  value: "GB_United Kingdom"
}, {
  key: "US_United States",
  label: "United States",
  value: "US_United States"
}, {
  key: "UM_United States Minor Outlying Islands",
  label: "United States Minor Outlying Islands",
  value: "UM_United States Minor Outlying Islands"
}, {
  key: "UY_Uruguay",
  label: "Uruguay",
  value: "UY_Uruguay"
}, {
  key: "UZ_Uzbekistan",
  label: "Uzbekistan",
  value: "UZ_Uzbekistan"
}, {
  key: "VU_Vanuatu",
  label: "Vanuatu",
  value: "VU_Vanuatu"
}, {
  key: "VE_Venezuela",
  label: "Venezuela",
  value: "VE_Venezuela"
}, {
  key: "VN_Viet Nam",
  label: "Viet Nam",
  value: "VN_Viet Nam"
}, {
  key: "VG_Virgin Islands, British",
  label: "Virgin Islands, British",
  value: "VG_Virgin Islands, British"
}, {
  key: "VI_Virgin Islands, U.S.",
  label: "Virgin Islands, U.S.",
  value: "VI_Virgin Islands, U.S."
}, {
  key: "WF_Wallis and Futuna",
  label: "Wallis and Futuna",
  value: "WF_Wallis and Futuna"
}, {
  key: "EH_Western Sahara",
  label: "Western Sahara",
  value: "EH_Western Sahara"
}, {
  key: "YE_Yemen",
  label: "Yemen",
  value: "YE_Yemen"
}, {
  key: "ZM_Zambia",
  label: "Zambia",
  value: "ZM_Zambia"
}, {
  key: "ZW_Zimbabwe",
  label: "Zimbabwe",
  value: "ZW_Zimbabwe"
}]