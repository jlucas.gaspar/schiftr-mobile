import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { TouchableOpacity } from 'react-native-gesture-handler';

export const Container = styled.View`
  margin: ${RFValue(5)}px ${RFValue(10)}px;
`;

export const CommentWrapper = styled.View`
  flex-direction: row;
`;

export const UserWrapper = styled.View`
  flex-direction: row;
`;

export const UserInfo = styled(TouchableOpacity)``;

export const Username = styled.Text`
  font-size: ${RFValue(12.5)}px;
  font-family: ${({ theme }) => theme.fonts.nunito.bold};
`;

export const Avatar = styled.Image`
  width: ${RFValue(18)}px;
  height: ${RFValue(18)}px;
  border-radius: 100px;
  margin-right: 5px;
`;

export const CommentStringWrapper = styled.View`
  flex: 1;
  margin: 0 ${RFValue(5)}px;
  margin-top: ${1}px;
`;

export const CommentString = styled.Text``;

export const SeeMoreText = styled.Text`
  color: #555;
`;

export const LikeArea = styled.View`
  justify-content: center;
  align-items: center;
`;

export const LikeTouchable = styled(TouchableOpacity)`
  margin: auto ${RFValue(2)}px;
  margin-left: auto;
`;

export const NumberOfLikes = styled.Text`
  color: #222;
  font-size: ${RFValue(11)}px;
`;

export const Date = styled.Text`
  /* margin: ${RFValue(2)}px 0; */
  color: #666;
`;