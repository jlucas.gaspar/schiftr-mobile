import { string, object, number } from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

export const editUserResolver = yupResolver(object().shape({
  name: string().required(),
  birthday: string().matches(/^\d{2}\/\d{2}\/\d{4}$/, { message: 'Invalid date format. Correct is MM/DD/YYYY' }).optional(),
  gender: string().optional(),
  country: string().optional(),
  city: string().optional(),
  state: string().optional(),
  phone: string().optional(),
  zipCode: number().typeError('Zip Code must be a number.').optional(),
}));