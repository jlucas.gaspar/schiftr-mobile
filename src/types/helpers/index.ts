import { ReactNode } from 'react';

export type MakeFunction<Input, Output> = (input: Input) => Promise<Output | void>;

export type Dates = {
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date | null;
}

export type Id = {
  id: string;
}

export type WithChildren<T = {}> = T & {
  children?: ReactNode;
}