import { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { followService } from '../../services/follow';
import { postService } from '../../services/post';
import { userService } from '../../services/user';
import { Post } from '../../types/Post';
import { User } from '../../types/User';

type UserProfileFeed = User.Api.Res.GetUserByUsername;

type Post = {
  id: string;
  imageUri: string;
}

export const useProfileFeed = (username: string) => {
  const { username: myUsername } = useSelector(state => state.user.user);
  const [isLoading, setLoading] = useState(true);
  const [user, setUser] = useState<UserProfileFeed>();
  const [posts, setPosts] = useState<Post[]>();

  const fetchUserAndPhotos = async () => {
    const userResult = await userService.getUserByUsername({ username });

    if (userResult) {
      const userFollows = await followService.ensureIsFollowing({
        userFollowedId: userResult.id
      });

      const followersNumber = await followService.getNumberOfFollowers({
        userId: userResult.id
      });

      const followingNumber = await followService.getNumberOfFollowing({
        userId: userResult.id
      });

      setUser({
        ...userResult,
        youFollow: (userFollows && userFollows.isActive) ? true : false,
        followersNumber,
        followingNumber
      });

      const result = await postService.getAllPostsOfAnUser({ userId: userResult.id });
      if (result) {
        const minimunNumberOfPosts = 12;

        let numberOfPosts = 0;
        const parsedPosts = result.posts.map((post, index) => {
          numberOfPosts = index + 1;
          return {
            imageUri: post.imagesUrl[0],
            id: post.id
          }
        });

        if (numberOfPosts < minimunNumberOfPosts) {
          const missingPostsToCompleteAFeed = minimunNumberOfPosts - numberOfPosts;

          for (let i = 0; i < missingPostsToCompleteAFeed; i++) {
            parsedPosts.push({ id: `${i}`, imageUri: '' });
          }
        }

        setPosts(parsedPosts);
      }
    }

    setLoading(false);
  }

  useEffect(() => {
    fetchUserAndPhotos();
  }, []);

  const handleFollow = async (isFollowing: boolean) => {
    if (isFollowing) {
      setUser(user => user && ({
        ...user,
        youFollow: true,
        followersNumber: user.followersNumber + 1
      }));

      await followService.follow({ userFollowedId: user!.id });
    } else {
      setUser(user => user && ({
        ...user,
        youFollow: false,
        followersNumber: user.followersNumber - 1
      }));
      await followService.unfollow({ userFollowedId: user!.id });
    }
  }

  return {
    isLoading,
    user,
    posts,
    handleFollow,
    myUsername
  }
}