import { Comment } from '../types/Comment';
import { Post } from '../types/Post';
import { makeApiRequest } from './helpers/axios';
import { handleServiceError } from './helpers/errorHandler';

type CommentService = {
  createComment: Post.Api.CreateComment;
  getAllCommentsByPostId: Post.Api.GetAllCommentsByPostId;
  addLike: (input: Comment.Http.Request.AddLike) => Promise<Comment.Http.Response.AddLike>;
  removeLike: (input: Comment.Http.Request.RemoveLike) => Promise<Comment.Http.Response.RemoveLike>;
}

export const commentService: CommentService = {
  createComment: async ({ commentString, postId, answeredCommentId }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: '/comment',
        data: { commentString, postId, answeredCommentId }
      });
    } catch (error) {
      handleServiceError(error, 'createComment');
    }
  },

  getAllCommentsByPostId: async ({ postId }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/comment/by-post/${postId}`
      });
    } catch (error) {
      handleServiceError(error, 'getAllCommentsByPostId');
    }
  },

  addLike: async ({ id }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: `/comment/add-like/${id}`
      });
    } catch (error) {
      handleServiceError(error, 'addLike');
    }
  },

  removeLike: async ({ id }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: `/comment/remove-like/${id}`
      });
    } catch (error) {
      handleServiceError(error, 'removeLike');
    }
  },
}