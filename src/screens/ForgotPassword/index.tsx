import React from 'react';
import { Touchable, Keyboard } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { Space } from '../../components';
import { Header, ForgotPasswordForm } from './components';
import { useForgotPassword } from './hooks';
import { Container, Title, Description, Content, ForgotPasswordBackground } from './styles';

export const ForgotPassword: React.FC = () => {
  const { control, errors, isLoading, handleForgotPassword } = useForgotPassword();

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss}>
      <Container>
        <Header />

        <Content>
          <Title>Forgot password?</Title>

          <Description>
            Please enter your e-mail address and we will send your password by e-mail immediately.
          </Description>

          <Space size={50} />

          <ForgotPasswordForm
            control={control}
            errors={errors}
            onSubmit={handleForgotPassword}
            isLoading={isLoading}
          />
        </Content>

        <ForgotPasswordBackground />
      </Container>
    </TouchableWithoutFeedback>
  );
}