import getStartedBackground from './get-started.png';
import signInBackground from './bike-sign-in.png';
import signUpBackground from './sign-up.png';
import forgotPasswordBackground from './forgot-password.png';

export const userWithoutAvatarUrl = 'https://cuidadospelavida.com.br/images/no_image_especialista.png';
export {
  forgotPasswordBackground,
  getStartedBackground,
  signInBackground,
  signUpBackground
}