import React, { useState } from 'react';
import { KeyboardAvoidingView, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/core';
import { AntDesign, Entypo } from '@expo/vector-icons';

import { RoutesList } from '../../../../routes/constants';
import { postService } from '../../../../services/post';
import { Button, Header, Space } from '../../../../components';

import { useCreatePostContext } from '../../context';
import { PhotoImage } from '../PhotoImage';

import { Input, LocationText, ChangePhotoIcon, ButtonWrapper, Label, LocationWrapper } from './styles';

export const FinishCreation = () => {
  const [description, setDescription] = useState<string | undefined>(undefined);
  const [isLoading, setLoading] = useState(false);
  const { photo, setStep, location, setPhoto, setLocation } = useCreatePostContext();
  const { navigate, goBack } = useNavigation();

  const handleSubmit = async () => {
    if (!photo?.uri) {
      Alert.alert('You must select a photo before creating a post.');
      return;
    }

    setLoading(true);

    const fileUrl = await postService.uploadPhoto({ fileUri: photo.uri });

    if (!fileUrl) {
      return setLoading(false);
    }

    await postService.createPost({
      location: location,
      text: description,
      imagesUrl: [fileUrl],
      isPrivate: false
    });

    // Comentado pois o usuario nao ve o seu proprio post
    // if (createdPost) {
    //   dispatch(postActions.setFeed([createdPost, ...feed]));
    // }

    setLoading(false);
    navigate(RoutesList.Feed as any);
  };

  const handleGoBack = () => {
    setLocation('');
    setPhoto(undefined);
    setStep('selectPhotoFromGalleryOrTakeFromCamera');
    goBack();
  }

  return (
    <>
      <Header
        goBackFunction={handleGoBack}
        actionFunction={() => setStep('selectPhotoFromGalleryOrTakeFromCamera')}
        actionIcon={(
          <ChangePhotoIcon>
            <Entypo name="camera" color="white" size={23} />
          </ChangePhotoIcon>
        )}
      />

      <ScrollView>
        <KeyboardAvoidingView behavior="position">
          <Space size={10} />

          <PhotoImage 
            photoUri={photo ? photo.uri : undefined}
            onSelectImage={() => setStep('selectPhotoFromGalleryOrTakeFromCamera')}
          />

          <Space size={20} />

          <Label>Subtitle</Label>
          <Input
            placeholder="Place a caption..."
            placeholderTextColor="#333"
            onChangeText={text => setDescription(text)}
            value={description}
            multiline
          />

          <Space size={20} />

          <LocationWrapper>
            <Label>{location ? 'Change' : 'Add'} location</Label>
            <AntDesign name="pluscircle" size={20} color="black" onPress={() => setStep('addLocation')} />
          </LocationWrapper>
          <LocationText>{location}</LocationText>

          <ButtonWrapper>
            <Button text="POST" color="black" onPress={handleSubmit} isLoading={isLoading} />
          </ButtonWrapper>
        </KeyboardAvoidingView>

        <Space size={150} />
      </ScrollView>
    </>
  );
}