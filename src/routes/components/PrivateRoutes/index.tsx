import React from 'react';
import { useTheme } from 'styled-components';
import { MaterialCommunityIcons, FontAwesome, Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { RoutesList } from '../../constants';
import * as ComponentScreen from '../../../screens';
import { makeTabBarNavigatorScreenOptions, tabBarHidden } from './utils';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export const PrivateRoutes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }}>
        <Stack.Screen
          name="Tabs"
          component={TabRoutes}
        />

        <Stack.Screen
          name={RoutesList.PostDetails}
          component={ComponentScreen.PostDetails}
        />

        <Stack.Screen
          name={RoutesList.ProfileFeed}
          component={ComponentScreen.ProfileFeed}
        />

        <Stack.Screen
          name={RoutesList.MyProfile}
          component={ComponentScreen.MyProfile}
        />

        <Stack.Screen
          name={RoutesList.EditProfile}
          component={ComponentScreen.EditProfile}
        />

        <Stack.Screen
          name={RoutesList.ChangePassword}
          component={ComponentScreen.ChangePassword}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const TabRoutes = () => {
  const theme = useTheme();

  return (
    <Tab.Navigator screenOptions={makeTabBarNavigatorScreenOptions(theme)}>
      <Tab.Screen
        name="InitialHidden"
        component={ComponentScreen.Feed}
        options={tabBarHidden}
      />

      {/* <Tab.Screen
        name={RoutesList.Calendar}
        component={ComponentScreen.Building}
        options={{
          tabBarIcon: ({ color, focused }) => {
            return focused
              ? <Ionicons name="md-calendar" color={color} size={30} />
              : <Ionicons name="md-calendar-outline" color={color} size={30} />
          }
        }}
      />  */}

      <Tab.Screen
        name={RoutesList.CreatePost}
        component={ComponentScreen.CreatePost}
        options={{
          tabBarIcon: ({ color, focused }) => {
            return focused
              ? <MaterialCommunityIcons name="camera-plus" color={color} size={30} />
              : <MaterialCommunityIcons name="camera-plus-outline" color={color} size={30} />
          }
        }}
      />

      <Tab.Screen
        name={RoutesList.Feed}
        component={ComponentScreen.Feed}
        options={{
          tabBarIcon: ({ color, focused }) => {
            return focused
              ? <Ionicons name="home" color={color} size={30} />
              : <Ionicons name="home-outline" color={color} size={30} />
          }
        }}
      />

      {/* <Tab.Screen
        name={RoutesList.Building}
        component={ComponentScreen.Building}
        options={{
          tabBarIcon: ({ color, focused }) => {
            return focused
              ? <Ionicons name="bookmark" color={color} size={30} />
              : <Ionicons name="bookmark-outline" color={color} size={30} />
          }
        }}
      /> */}

      <Tab.Screen
        name={RoutesList.MyProfile}
        component={ComponentScreen.MyProfile}
        options={{
          tabBarIcon: ({ color, focused }) => {
            return focused
              ? <FontAwesome name="user" color={color} size={30} />
              : <FontAwesome name="user-o" color={color} size={30} />
          }
        }}
      />
    </Tab.Navigator>
  );
}