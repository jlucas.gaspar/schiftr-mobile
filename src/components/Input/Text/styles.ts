import styled from 'styled-components/native';
import { TextInput } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  width: 100%;
`;

export const StyledInput = styled(TextInput)`
  width: 100%;
  border-bottom-width: 1px;
  border-bottom-color: #999;
  margin-bottom: 20px;
  color: ${_ => _.theme.colors.text};
  font-size: ${RFValue(15)}px;
  padding: 5px 8px;
`;

export const Error = styled.Text`
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.error};
  font-family: ${({ theme }) => theme.fonts.nunito.regular};
  margin-top: -15px;
`;