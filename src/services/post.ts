import { Post } from '../types/Post';
import { makeApiRequest } from './helpers/axios';
import { handleServiceError } from './helpers/errorHandler';

type PostService = {
  createPost: Post.Api.Create;
  uploadPhoto: Post.Api.UploadPhoto;
  getAllPostsOfAnUser: Post.Api.GetAllPostsOfAnUser;
  getPostDetails: Post.Api.GetPostDetails;
  addLike: Post.Api.AddLike;
  removeLike: Post.Api.RemoveLike;
  createComment: Post.Api.CreateComment;
  getAllCommentsByPostId: Post.Api.GetAllCommentsByPostId;
  getAllPostsForUserFeed: Post.Api.GetAllPostsOfAnFeedUser;
}

export const postService: PostService = {
  createPost: async ({ location, text, imagesUrl, isPrivate }) => {
    try {
      await makeApiRequest({
        method: 'POST',
        url: '/post',
        data: { location, text, imagesUrl, isPrivate }
      })
    } catch (err) {
      handleServiceError(err, 'createPost')
    }
  },

  uploadPhoto: async ({ fileUri }) => {
    try {
      const s3PreSignedUrl = await makeApiRequest({ method: 'GET', url: '/post/upload-url' });

      const xhr = new XMLHttpRequest();
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {} // console.log('Upload OK');
          else throw new Error('Upload Fail');
        }
      }
      xhr.open('PUT', s3PreSignedUrl);
      const fileType = 'image/jpeg';
      xhr.setRequestHeader('Content-Type', fileType);
      xhr.send({ uri: fileUri, type: fileType });
      return s3PreSignedUrl.split('?')[0];
    } catch (error) {
      handleServiceError(error, 'uploadPhoto');
    }
  },

  getAllPostsOfAnUser: async ({ userId }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/post/by-user/${userId}`
      });
    } catch (error) {
      handleServiceError(error, 'getAllPostsOfAnUser');
    }
  },

  getPostDetails: async ({ postId }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/post/details/${postId}`
      });
    } catch (error) {
      handleServiceError(error, 'getPostDetails');
    }
  },

  addLike: async ({ postId }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: '/post/add-like',
        data: { postId }
      });
    } catch (error) {
      handleServiceError(error, 'addLike');
    }
  },

  removeLike: async ({ postId }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: '/post/remove-like',
        data: { postId }
      });
    } catch (error) {
      handleServiceError(error, 'removeLike');
    }
  },

  createComment: async ({ commentString, postId, answeredCommentId }) => { // TODO :: Ajeitar as mudancas de tirar o comment do post (em types tbm)
    try {
      return await makeApiRequest({
        method: 'POST',
        url: '/comment',
        data: { commentString, postId, answeredCommentId }
      });
    } catch (error) {
      handleServiceError(error, 'createComment');
    }
  },

  getAllCommentsByPostId: async ({ postId }) => { // TODO :: Ajeitar as mudancas de tirar o comment do post (em types tbm)
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/comment/by-post/${postId}`
      });
    } catch (error) {
      handleServiceError(error, 'getAllCommentsByPostId');
    }
  },

  getAllPostsForUserFeed: async () => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/feed`
      });
    } catch (error) {
      handleServiceError(error, 'getAllPostsForUserFeed');
    }
  },
}