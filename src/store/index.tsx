import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { RootReducer } from './reducers';

const store = createStore(RootReducer);
export type RootState = ReturnType<typeof RootReducer>;

export const ReduxProvider: React.FC = ({ children }) => (
  <Provider store={store}>
    {children}
  </Provider>
);