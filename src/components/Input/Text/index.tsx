import React from 'react';
import { TextInputProps } from 'react-native';
import { Control, Controller, DeepMap, FieldValues, FieldError } from 'react-hook-form';
import { StyledInput, Container, Error } from './styles';

type InputProps = TextInputProps & {
  control: Control;
  name: string;
  errors: DeepMap<FieldValues, FieldError>;
}

export const Input: React.FC<InputProps> = ({ control, name, errors, defaultValue, ...rest }) => (
  <Container>
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      render={({ field: { onChange, value } }) => (
        <StyledInput
          onChangeText={onChange}
          value={value}
          placeholderTextColor="#999"
          defaultValue={defaultValue}
          {...rest}
        />
      )}
    />

    {errors[name] && errors[name].message && (
      <Error>{errors[name].message}</Error>
    )}
  </Container>
);