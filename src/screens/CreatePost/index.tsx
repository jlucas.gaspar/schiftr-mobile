import React from 'react';
import { CreatePostContextProvider } from './context';
import { CreatePostView } from './view';

export const CreatePost = () => (
  <CreatePostContextProvider>
    <CreatePostView />
  </CreatePostContextProvider>
);