import React from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useTheme } from 'styled-components';
import { Header } from '../../components';
import { useBuilding } from './hooks';
import { Container, Content, IconWrapper, Text } from './styles';
import { Octicons } from '@expo/vector-icons';

export const Building = () => {
  const { goBack } = useBuilding();
  const { colors } = useTheme();

  return (
    <Container>
      <Header hasShadow />

      <Content>
        <IconWrapper>
          <Octicons
            name="tools"
            size={100}
            // color={colors.primary}
            color="black"
          />
        </IconWrapper>

        <Text size={20}>Building...</Text>
        <Text size={15}>We are running to build this feature for you!</Text>
      </Content>
    </Container>
  );
}