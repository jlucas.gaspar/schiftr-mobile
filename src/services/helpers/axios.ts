import axios from 'axios';
import { env } from '../../env';
import { AsyncStorageError } from '../../errors';
import { getJwtToken, getUser, setJwtToken } from '../../utils/asyncStorage';

type MethodUppercase = 'GET' | 'POST' | 'PUT' | 'DELETE';
type MethodLowerCase = 'get' | 'post' | 'put' | 'delete';
type MakeRequest = {
  url: string;
  method: MethodUppercase;
  data?: any;
}

export const makeApiRequest = async ({ method, url, data: body }: MakeRequest) => {
  const baseURL = env.apiUrl;
  const methodLowerCase = method.toLowerCase() as MethodLowerCase;
  
  if (url.includes('/public')) {
    const api = axios.create({ baseURL });
    const { data } = await api[methodLowerCase](url, body);
    return data.data;
  }
  
  const jwtToken = await getJwtToken();
  if (!jwtToken) throw new AsyncStorageError('JWT Token not exists in AsyncStorage.');

  const { exp, token } = jwtToken;
  
  if (exp > Date.now()) {
    // Token expiration is OK, so we will make the request;
    const headers = { Authorization: `Bearer ${token}` }
    const api = axios.create({ baseURL, headers });
    const { data } = await api[methodLowerCase](url, body);
    return data.data;
  } else {
    // When the token is expired, we call the refresh token route and then run the request with the refreshed token;
    const user = await getUser();
    if (!user) throw new AsyncStorageError('User not found in AsyncStorage.');

    const firstApi = axios.create({ baseURL });
    const firstResult = await firstApi.get(`/auth/public/refresh-token/${user.id}`);
    const { token, exp } = firstResult.data.data;

    await setJwtToken({ token, exp });

    const headers = { Authorization: `Bearer ${token}` }
    const secondApi = axios.create({ baseURL, headers });
    const secondResult = await secondApi[methodLowerCase](url, body);
    return secondResult.data.data;
  }
}