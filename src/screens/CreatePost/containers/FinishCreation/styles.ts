import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Label = styled.Text`
  font-size: ${RFValue(15)}px;
  font-family: ${({ theme }) => theme.fonts.nunito.bold};
  margin: auto 10px;
`;

export const Input = styled.TextInput`
  margin: 0 10px;
  font-size: ${RFValue(13)}px;
  background-color: #C4C4C450;
  padding: 10px;
  border-radius: 10px;
`;

export const LocationWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const LocationText = styled.Text`
  font-size: ${RFValue(14)}px;
  font-family: ${({ theme }) => theme.fonts.nunito.regular};
  color: #222;
  margin: 0 10px;
  margin-bottom: 20px;
`;

export const ButtonWrapper = styled.View`
  width: 90%;
  margin: auto;
`;

export const ChangePhotoIcon = styled.View`
  width: ${RFValue(40)}px;
  height: ${RFValue(40)}px;
  border-radius: ${RFValue(100)}px;
  background-color: gray;
  justify-content: center;
  align-items: center;
`;