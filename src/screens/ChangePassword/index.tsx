import React from 'react';
import { Header, Space } from '../../components';

import { ChangePasswordForm } from './components';
import { useChangePasswordForm } from './hooks';
import { Container, Title, Form, SaveButton } from './styles';

export const ChangePassword = () => {
  const { control, errors, handleUpdatePassword, isLoading } = useChangePasswordForm();
  return (
    <>
      <Header hasShadow />

      <Container>
        <Space size={50} />

        <Title>Change Password</Title>

        <Form>
          <ChangePasswordForm control={control} errors={errors} />
        </Form>

        <SaveButton text="SAVE" onPress={handleUpdatePassword} isLoading={isLoading} />
      </Container>
    </>
  );
}