declare module '*.png' {
  import React from 'react';
  import { ImageProps } from 'react-native'
  const content: ImageProps;
  export default content;
}