import React from 'react';
import { FlatList, RefreshControl, View } from 'react-native';
import { Header, Loading } from '../../components';
import { Post } from './components/Post';
import { useFetchFeedPosts } from './hooks';

export const Feed: React.FC = () => {
  const { isLoading, posts, fetchPosts, handleScroll } = useFetchFeedPosts();

  if (isLoading && !posts.length) return <Loading />;
  return (
    <>
      <Header hasShadow />

      <FlatList
        showsVerticalScrollIndicator={false}
        refreshControl={<RefreshControl refreshing={isLoading} onRefresh={() => fetchPosts()} />} // onRefresh={async () => await fetchPosts()}
        onScroll={handleScroll}
        data={posts}
        keyExtractor={(post) => post.id}
        renderItem={({ item }) => <Post data={item} />}
        ListHeaderComponent={<View />}
        ListFooterComponent={<View />}
        ListHeaderComponentStyle={{ marginTop: 40 }}
        ListFooterComponentStyle={{ marginBottom: 90 }}
      />
    </>
  );
}