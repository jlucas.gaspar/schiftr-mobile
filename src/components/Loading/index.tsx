import React from 'react';
import { ActivityIndicator } from 'react-native';
import { Container } from './styles';

type Props = {
  color?: string;
}

export const Loading = ({ color }: Props) => (
  <Container>
    <ActivityIndicator size="large" color={color} />
  </Container>
);