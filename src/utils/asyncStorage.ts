import AsyncStorage from '@react-native-async-storage/async-storage';
import { Auth } from '../types/Auth';
import { User } from '../types/User';

type Token = Auth.Helpers.JwtTokenWithExp;
type User = User.Model.Complete;

export const getJwtToken = async (): Promise<Token | void> => {
  const jwtTokenObject = await AsyncStorage.getItem('@schiftr::jwtToken');
  if (!jwtTokenObject) return;

  return JSON.parse(jwtTokenObject);
}

export const setJwtToken = async ({ exp, token }: Token) => {
  const jwtTokenObject = JSON.stringify({ exp, token });
  await AsyncStorage.setItem('@schiftr::jwtToken', jwtTokenObject);
}

export const setUser = async (user: User) => {
  await AsyncStorage.setItem('@schiftr::user', JSON.stringify(user));
}

export const getUser = async (): Promise<User | null> => {
  const user = await AsyncStorage.getItem('@schiftr::user');
  if (!user) return null;
  else return JSON.parse(user);
}