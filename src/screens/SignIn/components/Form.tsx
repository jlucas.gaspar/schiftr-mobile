import React from 'react';
import { Control, FieldValues, DeepMap, FieldError } from 'react-hook-form';
import { Input, Button, PasswordInput } from '../../../components';
import { Form, Space, Text } from './Form.styles';

type Props = {
  isLoading: boolean;
  onSubmit: () => any;
  control: Control<FieldValues>;
  errors: DeepMap<FieldValues, FieldError>;
  goToForgotPasswordScreen: () => void;
}

export const SignInForm = ({ isLoading, onSubmit, control, errors, goToForgotPasswordScreen }: Props) => {
  return (
    <Form>
      <Input placeholder="E-mail" name="email" control={control} errors={errors} keyboardType="email-address" />
      <Space size={20} />
      <PasswordInput placeholder="Password" name="password" control={control} errors={errors} />
      <Text onPress={goToForgotPasswordScreen}>Forgot password?</Text>

      {/* <Checkbox text="Remember me" isChecked={rememberMe} onValueChange={isChecked => setRememberMe(isChecked)} />
      <Space size={20} /> */}

      <Button text="SIGN IN" onPress={onSubmit} isLoading={isLoading} />
    </Form>
  );
}