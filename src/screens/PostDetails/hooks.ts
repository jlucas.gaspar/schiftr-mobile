import { useState, useEffect } from 'react';
import { useNavigation } from '@react-navigation/core';
import { useDispatch, useSelector } from 'react-redux';
import * as postActions from '../../store/actions/post';
import { RootState } from '../../store';
import { Post } from '../../types/Post';
import { postService } from '../../services/post';
import { userService } from '../../services/user';
import { commentService } from '../../services/comment';

export const usePostDetails = (postId: string) => {
  const { detailedPost } = useSelector((state: RootState) => state.post);
  const [isLoading, setLoading] = useState(true);
  const dispatch = useDispatch();

  const fetchPost = async () => {
    const postFromApi = await postService.getPostDetails({ postId });

    if (postFromApi) {
      const { post } = postFromApi;
      const user = await userService.getMe({ userId: post.userId });


      // dispatch(postActions.setDetailedPost({
      //   ...post,
      //   imagesUrl: post.imagesUrl,
      //   // userAvatar: user!.avatarImageUrl!,
      //   // userUsername: user!.username,
      //   // comments: []
      // }));

      const comments = await postService.getAllCommentsByPostId({ postId });

      dispatch(postActions.setDetailedPost({
        comments: comments || [],
        userAvatar: user!.avatarImageUrl as string,
        userUsername: user!.username,
        id: post.id,
        commentsNumber: post.commentsNumber,
        imagesUrl: post.imagesUrl,
        isPrivate: post.isPrivate,
        likes: post.likes,
        userId: post.userId,
        location: post.location,
        text: post.text,
        createdAt: post.createdAt,
        updatedAt: post.updatedAt,
        deletedAt: post.deletedAt
      }));
    }
  
    setLoading(false);
  }

  useEffect(() => {
    fetchPost();
  }, []);

  return { post: detailedPost, isLoading }
}

export const useComment = () => {
  const [showCompleteBigComment, setShowCompleteBigComment] = useState(false);
  const { detailedPost } = useSelector((state: RootState) => state.post);
  const { user } = useSelector((state: RootState) => state.user);
  const { navigate } = useNavigation();
  const dispatch = useDispatch();

  const handleShowLess = () => setShowCompleteBigComment(false);
  const handleShowMore = () => setShowCompleteBigComment(true);
  
  const handleAddLike = async (clickedCommentId: string) => {
    const updateUsersCommentAndLikesArray = (comment: Post.Helpers.Comment): Post.Helpers.Comment => ({
      ...comment,
      usersLikedIdArray: [...comment.usersLikedIdArray, user.id]
    });

    dispatch(postActions.setDetailedPost({
      ...detailedPost,
      comments: detailedPost.comments.map(
        comm => (comm.id === clickedCommentId) ? updateUsersCommentAndLikesArray(comm) : comm
      )
    }));

    await commentService.addLike({ id: clickedCommentId });
  }

  const handleRemoveLike = async (clickedCommentId: string) => {
    const updateUsersCommentAndLikesArray = (comment: Post.Helpers.Comment): Post.Helpers.Comment => ({
      ...comment,
      usersLikedIdArray: comment.usersLikedIdArray.filter((likedId) => likedId !== user.id)
    });

    dispatch(postActions.setDetailedPost({
      ...detailedPost,
      comments: detailedPost.comments.map(
        comm => comm.id === clickedCommentId ? updateUsersCommentAndLikesArray(comm) : comm
      )
    }));

    await commentService.removeLike({ id: clickedCommentId });
  }

  return {
    user,

    commentLike: {
      add: handleAddLike,
      remove: handleRemoveLike,
    },
    
    commentText: {
      showMore: handleShowMore,
      showLess: handleShowLess,
      showCompleteBigComment
    }
  }
}