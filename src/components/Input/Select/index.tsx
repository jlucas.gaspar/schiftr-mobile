import React from 'react';
import Picker, { PickerSelectProps } from 'react-native-picker-select';
import { StyleSheet } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Control, Controller, DeepMap, FieldValues, FieldError } from 'react-hook-form';
// import { Picker, PickerProps } from '@react-native-picker/picker'
import { Container, Error } from './styles';

type SelectInputProps = Omit<PickerSelectProps, 'onValueChange' | 'placeholder'> & {
  control: Control;
  name: string;
  errors: DeepMap<FieldValues, FieldError>;
  defaultValue?: {
    label: any;
    value: any;
  }
  placeholder?: {
    label: any;
    value: any;
  }
}

export const SelectInput: React.FC<SelectInputProps> = ({ control, name, errors, defaultValue, placeholder, ...rest }) => (
  <Container>
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      render={({ field: { onChange, value } }) => (
        <Picker
          {...rest}
          placeholder={defaultValue?.value ? defaultValue : placeholder}
          onValueChange={(value) => value ? onChange(value) : onChange(undefined)}
          value={value}
          style={{ ...pickerSelectStyles }}
        />
      )}
    />

    {errors[name] && errors[name].message && (
      <Error>{errors[name].message}</Error>
    )}
  </Container>
);

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    width: '100%',
    borderBottomWidth: 1,
    borderBottomColor: '#999',
    marginBottom: 20,
    color: '#FFF',
    paddingVertical: 5,
    paddingHorizontal: 8,
    fontSize: RFValue(15),
  },
  placeholder: {
    color: '#999'
  }
});