import { useCallback, useEffect, useState } from 'react';

export const useBase = () => {
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {}, []);

  const handle = useCallback(() => {}, []);

  return {
    isLoading,
    handle
  }
}