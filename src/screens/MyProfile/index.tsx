import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { RootState } from '../../store';
import * as userActions from '../../store/actions/user';
import { Header, Loading, Space } from '../../components';
import { RoutesList } from '../../routes/constants';
import {
  Container, UserName, UserPhoto, RoundWhite, EditUserIcon, UserCity,
  ClickableButton, LockIcon, ClickableText, ClickableButtons
} from './styles';
import { useChangePhoto } from './hooks';
import { TouchableOpacity } from 'react-native-gesture-handler';

export const MyProfile: React.FC = () => {
  const { handleSelectPhoto, isLoading, photoUri } = useChangePhoto();
  const { user } = useSelector((state: RootState) => state.user);
  const { navigate } = useNavigation();
  const dispatch = useDispatch();

  const handleShowCorrectName = () => {
    if (user.name) return user.name;
    else if (user.username) return user.username;
    else return user.email;
  }

  const handleShowCorrectCityAndCountry = () => {
    if (user.city) return user.city;
    else return 'No city registered';
  }

  if (isLoading) return <Loading />;
  return (
    <>
      <Header hasShadow />

      <Container>
        <Space size={20} />
        {user.avatarImageUrl && (
          <TouchableOpacity onPress={handleSelectPhoto}>
            <UserPhoto source={{ uri: photoUri }} />
          </TouchableOpacity>
        )}
        {!user.avatarImageUrl && (
          <RoundWhite onPress={handleSelectPhoto}>
            <EditUserIcon name="add-a-photo" size={70} />
          </RoundWhite>
        )}

        <UserName>{handleShowCorrectName()}</UserName>
        <UserCity>{handleShowCorrectCityAndCountry()}</UserCity>

        <ClickableButtons>
          <ClickableButton onPress={() => navigate(RoutesList.EditProfile as any)}>
            <LockIcon name="edit-3" />
            <ClickableText>Edit Profile</ClickableText>
          </ClickableButton>

          <ClickableButton onPress={() => navigate(RoutesList.ChangePassword as any)}>
            <LockIcon name="lock" />
            <ClickableText>Change Password</ClickableText>
          </ClickableButton>

          <ClickableButton onPress={() => dispatch(userActions.logout())}>
            <LockIcon name="power" />
            <ClickableText>Sign Out</ClickableText>
          </ClickableButton>
        </ClickableButtons>

        {/* <Button text="LOGOUT" onPress={() => dispatch(userActions.logout())} /> */}
      </Container>
    </>
  );
}