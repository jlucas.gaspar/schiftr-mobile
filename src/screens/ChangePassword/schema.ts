import { SchemaOf, string, object, ref } from 'yup';

export type FormSchema = {
  oldPassword: string;
  newPassword: string;
  newPasswordConfirmation: string;
}

export const schema: SchemaOf<FormSchema> = object().shape({
  oldPassword: string().required('Current Password is required.'),
  newPassword: string().required('New Password is required.'),
  newPasswordConfirmation: string().required('New Password Confirmation is required.').oneOf([ref('password'), null], 'Passwords must match.')
});