import { useNavigation } from '@react-navigation/core';

export const useBuilding = () => {
  const navigation = useNavigation();

  const goBack = () => navigation.goBack();

  return {
    goBack
  }
}