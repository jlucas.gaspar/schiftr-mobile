export enum RoutesList {
  // Public Routes
  GetStarted = 'GetStarted',
  ResetPassword = 'ResetPassword',
  ForgotPassword = 'ForgotPassword',
  SignIn = 'SignIn',
  SignUp = 'SignUp',

  // Private Routes
  Feed = 'Feed',
  ProfileFeed = 'ProfileFeed',
  CreatePost = 'CreatePost',
  PostDetails = 'PostDetails',
  MyProfile = 'MyProfile',
  EditProfile = 'EditProfile',
  ChangePassword = 'ChangePassword',
  Building = 'Building',
  Calendar = 'Calendar',
  Search = 'Search'
}