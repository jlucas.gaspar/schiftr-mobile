import { Auth } from './Auth';
import { MakeFunction, Dates } from './helpers';

export namespace User {
  export namespace Model {
    export type Complete = Dates & {
      id: string;
      name: string;
      email: string;
      username: string;
      provider: Helpers.Provider;
      providerId: string;
      avatarImageUrl?: string;
      gender?: string;
      phone?: string;
      birthday?: string;
      country?: string;
      city?: string;
      state?: string;
      zipCode?: number;
    }
    export type WithoutId = Omit<Complete, 'id'>;
    export type WithoutDates = Omit<Complete, 'createdAt' | 'updatedAt' | 'deletedAt'>;
    export type WithoutDatesAndId = Omit<WithoutDates, 'id'>;
    export type WithFollowInfo = Complete & { youFollow: boolean; }
  }

  export namespace FormData {
    export type UpdateUser = Partial<Model.WithoutDatesAndId>;
  }

  export namespace Api {
    export type UpdateUser = MakeFunction<Req.UpdateUser, Res.UpdateUser>;
    export type UploadPhoto = MakeFunction<Req.UploadPhoto, Res.UploadPhoto>;
    export type GetMe = MakeFunction<Req.GetMe, Res.GetMe>;
    export type GetUserByUsername = MakeFunction<Req.GetUserByUsername, Res.GetUserByUsername>;
    export namespace Req {
      export type UpdateUser = FormData.UpdateUser;
      export type UploadPhoto = { fileUri: string; };
      export type GetMe = { userId: string; }
      export type GetUserByUsername = { username: string; }
    }
    export namespace Res {
      export type UpdateUser = Model.Complete;
      export type UploadPhoto = string;
      export type GetMe = User.Model.Complete | undefined;
      export type GetUserByUsername = User.Model.WithFollowInfo & Helpers.FollowersAndFollowingNumber | undefined;
    }
  }

  export namespace Helpers {
    export type UserAndToken = Auth.Helpers.UserWithToken;
    export type Provider = 'google' | 'apple' | 'facebook' | 'email';
    export type FollowersAndFollowingNumber = { followersNumber: number; followingNumber: number; }
  }

  export namespace Store {
    export type State = {
      user: User.Model.Complete;
      isAuthenticated: boolean;
      token: Auth.Helpers.JwtTokenWithExp;
    }
    export enum ActionsName {
      Authenticate = 'Authenticate',
      Logout = 'Logout',
      SetUser = 'SetUser'
    }
    export namespace Actions {
      export type All = Authenticate | Logout | SetUser;
      export type Authenticate = { type: ActionsName.Authenticate; payload: Helpers.UserAndToken; }
      export type Logout = { type: ActionsName.Logout; }
      export type SetUser = { type: ActionsName.SetUser; payload: Partial<User.Model.Complete> }
    }
  }
}