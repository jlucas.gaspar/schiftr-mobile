import React from 'react';
import { Line, Text, Container, TextWrapper } from './styles';

type DividerProps = {
  text?: string;
  textColor?: string;
  lineColor?: string;
}

const DividerWithText: React.FC<DividerProps> = ({ text, textColor, lineColor }) => (
  <Container>
    <Line lineColor={lineColor} />
    <TextWrapper>
      <Text style={{ color: textColor ? textColor : 'white' }}>
        {text}
      </Text>
    </TextWrapper>
    <Line lineColor={lineColor} />
  </Container>
);

const DividerWithoutText: React.FC<DividerProps> = ({ lineColor }) => (
  <Container>
    <Line lineColor={lineColor} />
  </Container>
);

export const Divider: React.FC<DividerProps> = ({ text, textColor }) => (
  text ? <DividerWithText text={text} textColor={textColor} /> : <DividerWithoutText />
);