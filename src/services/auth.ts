import * as Google from 'expo-google-app-auth';
import { Alert } from 'react-native';
import { setJwtToken, setUser } from '../utils/asyncStorage';
import { Auth } from '../types/Auth';
import { env } from '../env';
import { makeApiRequest } from './helpers/axios';
import { handleServiceError } from './helpers/errorHandler';

type AuthService = {
  googleSignUp: Auth.Api.GoogleSignUp;
  googleSignIn: Auth.Api.GoogleSignIn;
  emailSignUp: Auth.Api.EmailSignUp;
  emailSignIn: Auth.Api.EmailSignIn;
  forgotPassword: Auth.Api.ForgotPassword;
  resetPassword: Auth.Api.ResetPassword;
  confirmResetPasswordReceivedToken: Auth.Api.ConfirmResetPasswordReceivedToken;
  refreshToken: Auth.Api.RefreshToken;
  updatePassword: Auth.Api.UpdatePassword;
}

const { iosClientId, androidClientId } = env;

export const authService: AuthService = {
  googleSignUp: async () => {
    try {
      const result = await Google.logInAsync({ iosClientId, androidClientId /* scopes: ['profiles', 'email'] */ });

      if (result.type !== 'success') {
        return Alert.alert('Something went wrong with your Google authentication.');
      }

      const { email, name, photoUrl: avatarImageUrl, id: providerId } = result.user;
      const provider = 'google';
      const username = email;

      const response = await makeApiRequest({
        method: 'POST',
        url: '/auth/public/signup',
        data: { email, name, avatarImageUrl, provider, username, providerId }
      });
      await setJwtToken(response.token);
      await setUser(response.user);
    } catch (err) {
      handleServiceError(err, 'googleSignUp');
    }
  },

  googleSignIn: async () => {
    try {
      const result = await Google.logInAsync({ iosClientId, androidClientId /* scopes: ['profiles', 'email'] */ });

      if (result.type !== 'success') {
        return Alert.alert('Something went wrong with your Google authentication.');
      }

      const { email, name, photoUrl: avatarImageUrl, id: providerId } = result.user;
      const provider = 'google';
      const username = email;

      const response = await makeApiRequest({
        method: 'POST',
        url: '/auth/public/login',
        data: { email, name, avatarImageUrl, provider, username, providerId }
      });
      await setJwtToken(response.token);
      await setUser(response.user);
      return response;
    } catch (err) {
      handleServiceError(err, 'googleSignIn');
    }
  },

  forgotPassword: async ({ email }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: '/auth/public/reset-password',
        data: { email }
      })
    } catch (err) {
      handleServiceError(err, 'forgotPassword');
    }
  },

  confirmResetPasswordReceivedToken: async ({ token }) => {
    try {
      return await makeApiRequest({
        method: 'GET',
        url: `/auth/public/reset-password-token/${token}`
      })
    } catch (err) {
      handleServiceError(err, 'confirmResetPasswordReceivedToken');
    }
  },

  emailSignIn: async ({ email, password }) => {
    try {
      const response = await makeApiRequest({
        method: 'POST',
        url: '/auth/public/login',
        data: { email, password, provider: 'email' }
      });
      await setJwtToken(response.token);
      await setUser(response.user);
      return response;
    } catch (err) {
      handleServiceError(err, 'emailSignIn');
    }
  },

  emailSignUp: async ({ password, email, passwordConfirmation, username }) => {
    try {
      const provider = 'email';
      const response = await makeApiRequest({
        method: 'POST',
        url: '/auth/public/signup',
        data: { password, email, passwordConfirmation, username, provider }
      });
      await setJwtToken(response.token);
      await setUser(response.user);
      return response;
    } catch (err) {
      handleServiceError(err, 'emailSignUp');
    }
  },

  refreshToken: async ({ userId }) => {
    try {
      const response = await makeApiRequest({
        method: 'GET',
        url: `/auth/public/refresh-token/${userId}`
      });
      await setJwtToken(response.token);
      await setUser(response.user);
      return response;
    } catch (err) {
      handleServiceError(err, 'refreshToken');
    }
  },

  resetPassword: async ({ email }) => {
    try {
      return await makeApiRequest({
        method: 'POST',
        url: '/auth/public/reset-password',
        data: { email }
      })
    } catch (error) {
      handleServiceError(error, 'resetPassword');
    }
  },

  updatePassword: async ({ password, passwordConfirmation, token }) => {
    try {
      const response = await makeApiRequest({
        method: 'POST',
        url: '/auth/public/update-password',
        data: { password, passwordConfirmation, token }
      });
      await setJwtToken(response.token);
      await setUser(response.user);
      return response;
    } catch (error) {
      handleServiceError(error, 'updatePassword');
    }
  }
}