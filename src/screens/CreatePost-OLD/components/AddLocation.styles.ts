import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  /* flex: 1; */
  align-items: center;
  justify-content: center;
  align-content: center;
`;

export const Inputwrapper = styled.View`
  margin: 0px 10px;
  padding: 10px;
  width: 100%;
  margin-top: 50px;
`;

export const LocationInput = styled.TextInput`
  border-radius: 10px;
  padding: 20px;
  background-color: #e1e1e1;
  font-family: ${({ theme }) => theme.fonts.nunito.medium};
  font-size: ${RFValue(14)}px;
  color: #000;
`;