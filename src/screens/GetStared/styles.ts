import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import { RFValue } from 'react-native-responsive-fontsize';
import { getStartedBackground } from '../../assets/images';
import { LogoIcon as Logo } from '../../assets/icons';

export const LogoIcon = styled(Logo).attrs({ width: 400 })``;

export const Container = styled.ImageBackground.attrs({ source: getStartedBackground })`
  flex: 1;
  justify-content: space-around;
`;

export const Header = styled.View``;

export const Subtitle = styled.View`
  margin-left: ${RFValue(20)}px;
  margin-top: ${RFValue(20)}px;
`;

export const Regular = styled.Text`
  margin-top: 10px;
  color: ${_ => _.theme.colors.text};
  font-family: ${_ => _.theme.fonts.raleway.regular};
  font-size: ${RFValue(35)}px;
`;

export const Strong = styled.Text`
  color: ${_ => _.theme.colors.primary};
  font-family: ${_ => _.theme.fonts.raleway.semiBold};
  font-size: ${RFValue(35)}px;
`;

export const Description = styled.Text`
  color: ${_ => _.theme.colors.text};
  font-size: ${RFValue(15)}px;
  margin-left: 20px;
  margin-right: 100px;
  margin-top: ${RFValue(120)}px;
`;

export const AuthButtons = styled.View`
  margin-top: ${RFValue(40)}px;
`;

export const Footer = styled.View``;

export const LoginLink = styled(RectButton)`
  margin: auto;
`;

export const LoginText = styled.Text`
  color: ${_ => _.theme.colors.text};
  font-family: ${_ => _.theme.fonts.nunito.bold};
  font-size: ${RFValue(14)}px;
`;