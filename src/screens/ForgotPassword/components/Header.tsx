import React from 'react';
import { useNavigation } from '@react-navigation/native';
import { Container, BackButton, BackIcon, TextButton, Space,
 LogoIcon
} from './Header.styles';

export const Header: React.FC = () => {
  const { goBack } = useNavigation();

  return (
    <Container>
      <Space>
        <BackButton onPress={() => goBack()}>
          <BackIcon name="chevron-left" />
          <TextButton>BACK</TextButton>
        </BackButton>
      </Space>

      <LogoIcon />

      <Space />
    </Container>
  );
}