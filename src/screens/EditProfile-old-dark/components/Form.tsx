import React from 'react';
import { Control, FieldValues, DeepMap, FieldError } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { Input, Button, SelectInput, Space } from '../../../components';
import { RootState } from '../../../store';
import { countries, genres } from '../constants';
import { Form } from './Form.styles';

type Props = {
  isLoading: boolean;
  onSubmit: () => any;
  control: Control<FieldValues>;
  errors: DeepMap<FieldValues, FieldError>;
}

export const EditUserForm = ({ control, errors, onSubmit, isLoading }: Props) => {
  const { user } = useSelector((state: RootState) => state.user);

  return (
    <Form>
      <Input
        control={control}
        errors={errors}
        defaultValue={user.name}
        name="name"
        placeholder="Full Name"
        autoCompleteType="name"
        textContentType="name"
      />

      <Input
        control={control}
        errors={errors}
        defaultValue={user.birthday}
        name="birthday"
        placeholder="Birthday - DD/MM/YYYY"
        keyboardType="numbers-and-punctuation"
      />

      <SelectInput
        control={control}
        errors={errors}
        name="gender"
        items={genres}
        defaultValue={{ label: user.gender, value: user.gender }}
        placeholder={{ label: 'Select a gender...', value: undefined }}
      />

      <SelectInput
        control={control}
        errors={errors}
        name="country" 
        items={countries}
        defaultValue={{ label: countries.find(c => c.value === user.country)?.label, value: user.country }}
        placeholder={{ label: 'Select a country...', value: undefined }}
      />

      <Input
        control={control}
        errors={errors}
        defaultValue={user.city}
        name="city"
        placeholder="City"
        autoCompleteType="street-address"
        textContentType="addressCity"
      />

      <Input
        control={control}
        errors={errors}
        defaultValue={user.state}
        name="state"
        placeholder="State"
        autoCompleteType="street-address"
        textContentType="addressState"
      />

      <Input
        control={control}
        errors={errors}
        defaultValue={user.zipCode ? String(user.zipCode) : undefined}
        name="zipCode"
        placeholder="Zipcode"
        keyboardType="numeric"
        autoCompleteType="postal-code"
        textContentType="postalCode"
      />

      <Space size={30} />

      <Button text="SAVE INFORMATION" onPress={onSubmit} isLoading={isLoading} />
    </Form>
  );
}