import { RFValue } from 'react-native-responsive-fontsize';
import styled from 'styled-components/native';
import { signInBackground } from '../../assets/images';

export const Container = styled.View`
  width: 100%;
  height: 100%;
  /* background-color: ${_ => _.theme.colors.background}; */
  background-color: #0A0C0F;
  color: ${_ => _.theme.colors.text};
`;

export const BikeBackground = styled.ImageBackground.attrs({ source: signInBackground })`
  width: 150%;
  height: 400px;
  margin-top: ${RFValue(-15)}px;
`;