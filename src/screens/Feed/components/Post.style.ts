import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const NumberOfCommentsWrap = styled.View`
  margin: 0 15px;
`;

export const NumberOfComments = styled.Text`
  font-family: ${({ theme }) => theme.fonts.nunito.medium};
  font-size: ${RFValue(12)}px;
  color: #444;
`;

export const DividerWrap = styled.View`
  margin-top: 15px;
  margin-bottom: 15px;
  align-items: center;
  justify-content: center;
`;

export const Divider = styled.View`
  height: 2px;
  background-color: #b9b9b9;
  width: 90%;
  border-radius: 100px;
`;