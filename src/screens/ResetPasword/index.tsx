import React from 'react';
import PinInput from 'react-native-otp';
import { StatusBar } from 'react-native';
import { Button, Space, PasswordInput } from '../../components';
import { useConfirmResetPasswordToken, useUpdatePassword } from './hooks';
import { Form, Container, Text } from './styles';
import { Header } from './components';

export const ResetPassword: React.FC = () => {
  const { canRenderUpdatePasswordForm, handleConfirmResetPasswordToken, isLoadingTokenForm, setToken, token } = useConfirmResetPasswordToken();
  const { passwordControl, passwordErrors, handleUpdatePassword, isLoadingPasswordForm } = useUpdatePassword(token);

  return (
    <Container>
      <StatusBar barStyle="dark-content" backgroundColor="transparent" translucent  />
      <Header />
      
      {!canRenderUpdatePasswordForm && (
        <>
          <Text>
            Write the numeric {'\n'}
            token sent to your {'\n'}
            e-mail.
          </Text>

          <Form>
            <PinInput
              onChange={(e: string) => setToken(e)}
              cellStyle={{ color: '#FFF' }}
              otpLength={6}
            />
            <Space size={25} />
            <Button text="SEND" onPress={handleConfirmResetPasswordToken} isLoading={isLoadingTokenForm} />
          </Form>
        </>
      )}

      {canRenderUpdatePasswordForm && (
        <>
          <Text>
            Write your {'\n'}
            new password.
          </Text>
          <Form>
            <PasswordInput control={passwordControl} errors={passwordErrors} name="password" placeholder="Password" />
            <PasswordInput control={passwordControl} errors={passwordErrors} name="passwordConfirmation" placeholder="Password Confirmation" />
            <Space size={25} />
            <Button text="UPDATE PASSWORD" onPress={handleUpdatePassword} isLoading={isLoadingPasswordForm} />
          </Form>
        </>
      )}
    </Container>
  );
}