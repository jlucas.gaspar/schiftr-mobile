import React from 'react';
import ReactNativeZoomableView from '@dudigital/react-native-zoomable-view/src/ReactNativeZoomableView';
import { WithChildren } from '../../types/helpers';

export const Zoomable = ({ children }: WithChildren) => {
  // const [finished, setFinished] = useState(false);

  // const handleFinish = () => {
  //   setFinished(true);
  //   setTimeout(() => setFinished(false), 200);
  // }

  return (
    // <>
    //   {finished ? (
    //     <>
    //       {children}
    //     </>
    //   ) : (
        <ReactNativeZoomableView
          maxZoom={1.2}
          minZoom={1}
          initialZoom={1}
          bindToBorders={true}
          pinchToZoomInSensitivity={10}
          pinchToZoomOutSensitivity={10}
          movementSensibility={5}
          // captureEvent={true}
          // onZoomEnd={handleFinish}
        >
          {children}
        </ReactNativeZoomableView>
      // )}
    // </>
  )
}