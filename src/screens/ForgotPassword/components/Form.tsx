import React from 'react';
import { Control, FieldValues, DeepMap, FieldError } from 'react-hook-form';
import { Input, Button } from '../../../components';
import { Form } from './Form.styles';

type Props = {
  isLoading: boolean;
  onSubmit: () => any;
  control: Control<FieldValues>;
  errors: DeepMap<FieldValues, FieldError>
}

export const ForgotPasswordForm = ({ isLoading, onSubmit, control, errors }: Props) => (
  <Form>
    <Input
      placeholder="E-mail"
      name="email"
      control={control}
      errors={errors}
    />

    <Button onPress={onSubmit} text="SEND" isLoading={isLoading} />
  </Form>
);