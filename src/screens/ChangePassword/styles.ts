import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Button } from '../../components';

export const Container = styled.View`
  height: 100%;
  width: 100%;
  background-color: #FFF;
`;

export const Form = styled.View`
  padding: 30px 40px;
  background-color: ${({ theme }) => theme.colors.green};
  margin: 40px 20px;
  border-radius: 15px;
  shadow-opacity: 0.6;
  shadow-radius: 3px;
  shadow-color: #111;
  shadow-offset: 0px 5px;
`;

export const Title = styled.Text`
  font-family: ${({ theme }) => theme.fonts.nunito.bold};
  font-size: ${RFValue(25)}px;
  margin: 0 auto;
`;

export const SaveButton = styled(Button)`
  border-radius: ${RFValue(20)}px;
  shadow-opacity: 0.6;
  shadow-radius: 3px;
  shadow-color: #111;
  shadow-offset: 0px 5px;
  margin: 30px auto;
  width: 90%;
`;