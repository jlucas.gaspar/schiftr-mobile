import React from 'react';
import { Keyboard, TouchableWithoutFeedback } from 'react-native';
import { Container, Title, BikeBackground } from './styles';
import { Header, SignUpForm } from './components';
import { useSignUp } from './hooks';
import { Button } from '../../components';

export const SignUp: React.FC = () => {
  const {
    setLoading, agree, errors, isLoading, control, handleGoogleSignUp, handleSignUp, isLoadingGoogleAuth, setAgree
  } = useSignUp();

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Container>
        <Header />

        <Title>
          Create your {'\n'}
          account
        </Title>

        {/* <Button onPress={() => setLoading(false)} text="njvcnjerwkj" /> */}

        <SignUpForm
          control={control}
          emailSubmit={handleSignUp}
          errors={errors}
          googleSubmit={handleGoogleSignUp}
          isLoading={isLoading}
          isLoadingGoogle={isLoadingGoogleAuth}
        />

        <BikeBackground />
      </Container>
    </TouchableWithoutFeedback>
  );
}