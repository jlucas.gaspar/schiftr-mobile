import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { HeartFilledIcon, HeartIcon } from '../../../assets/icons';
import { RoutesList } from '../../../routes/constants';
import { Post } from '../../../types/Post';
import { parseCreationDateToString, parseLikesNumber } from '../utils';
import {
  Container,
  CommentWrapper,
  Avatar,
  Username,
  CommentString,
  UserInfo,
  LikeArea,
  LikeTouchable,
  NumberOfLikes,
  CommentStringWrapper,
  SeeMoreText,
  Date,
  UserWrapper
} from './Comment.styles';
import { useComment } from '../hooks';

type Props = Pick<
  Post.Helpers.Comment,
  'commentString' | 'commentUserAvatar' | 'commentUserUsername' | 'createdAt' | 'usersLikedIdArray' | 'id'
>;

const MAX_LENGTH = 70;

export const Comment = ({
  commentUserUsername,
  commentUserAvatar,
  commentString,
  createdAt,
  usersLikedIdArray,
  id: commentId
}: Props) => {
  const { commentLike, commentText, user } = useComment();
  const { navigate } = useNavigation();

  const comentIsLiked = () => {
    if (usersLikedIdArray.find(userId => userId === user.id)) return true;
    else return false;
  }

  return (
    <Container>
      <CommentWrapper>
        <UserInfo onPress={() => navigate(RoutesList.ProfileFeed as never, { username: commentUserUsername } as never)}>
          <UserWrapper>
            <Avatar source={{ uri: commentUserAvatar }} />
            <Username>{commentUserUsername}</Username>
          </UserWrapper>
          <Date>
            {parseCreationDateToString(createdAt)}
          </Date>
        </UserInfo>

        <CommentStringWrapper>
          {commentString.length <= MAX_LENGTH && (
            <CommentString>{commentString}</CommentString>
          )}

          {commentString.length > MAX_LENGTH && (
            <>
              {commentText.showCompleteBigComment && (
                <>
                  <CommentString>{commentString}</CommentString>
                  <SeeMoreText onPress={commentText.showLess}>see less...</SeeMoreText>
                </>
              )}
              {!commentText.showCompleteBigComment && (
                <>
                  <CommentString>{commentString.slice(0, MAX_LENGTH)}</CommentString>
                  <SeeMoreText onPress={commentText.showMore}>see more...</SeeMoreText>
                </>
              )}
            </>
          )}
        </CommentStringWrapper>
        
        <LikeArea>
          {comentIsLiked()
            ? (
              <LikeTouchable onPress={() => commentLike.remove(commentId)}>
                <HeartFilledIcon height={15} />
              </LikeTouchable>
            ) : (
              <LikeTouchable onPress={() => commentLike.add(commentId)}>
                <HeartIcon height={15} />
              </LikeTouchable>
            )
          }

          <NumberOfLikes>{parseLikesNumber(usersLikedIdArray.length)}</NumberOfLikes>
        </LikeArea>
      </CommentWrapper>
      
      {/* <SeeMoreText>{usersLikedIdArray.length}</SeeMoreText> */}
    </Container>
  );
}