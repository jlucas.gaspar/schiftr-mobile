import { User } from '../../types/User';

type Authenticate = User.Store.Actions.Authenticate;
type Logout = User.Store.Actions.Logout;
type SetUser = User.Store.Actions.SetUser;
type UserAndToken = User.Helpers.UserAndToken;

export const authenticate = ({ token, user }: UserAndToken): Authenticate => ({
  type: User.Store.ActionsName.Authenticate,
  payload: { user, token }
});

export const logout = (): Logout => ({
  type: User.Store.ActionsName.Logout
});

export const setUser = (user: Partial<User.Model.Complete>): SetUser => ({
  type: User.Store.ActionsName.SetUser,
  payload: user
});