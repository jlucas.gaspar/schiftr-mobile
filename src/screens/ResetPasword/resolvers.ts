import { string, object, ref, number } from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';

export const confirmTokenResolver = yupResolver(object().shape({
  token: number().typeError('Token must be a number.').required('Token is required.')
}));

export const updatePasswordResolver = yupResolver(object().shape({
  password: string().required('Password is required.'),
  passwordConfirmation: string().required('Password confirmation is required.').oneOf([ref('password'), null], 'Passwords must match.')
}));