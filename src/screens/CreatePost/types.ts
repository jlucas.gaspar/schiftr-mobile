import { Dispatch, SetStateAction } from 'react';

export namespace CreatePost {
  export type ContextState = {
    photo: Photo | undefined;
    setPhoto: Dispatch<SetStateAction<CreatePost.Photo | undefined>>;
    step: Step;
    setStep: Dispatch<SetStateAction<CreatePost.Step>>;
    location: string | undefined;
    setLocation: Dispatch<SetStateAction<string | undefined>>;
  }

  export type Photo = {
    uri: string;
    width: number;
    height: number;
    type?: 'image';
    base64?: string;
    exif?: {
      [key: string]: any;
    };
  }

  export type Step = 'selectPhotoFromGalleryOrTakeFromCamera' | 'addLocation' | 'finishCreate';
}