import styled from 'styled-components/native';
import { signUpBackground } from '../../assets/images';

export const Container = styled.View`
  height: 100%;
  width: 100%;
  background-color: ${_ => _.theme.colors.background};
`;

export const Form = styled.View`
  padding: 40px;
`;

export const BikeBackground = styled.ImageBackground.attrs({ source: signUpBackground })`
  z-index: -1;
  margin-top: -100px;
  height: 400px;
`;