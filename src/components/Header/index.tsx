import React from 'react';
import { StatusBar } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { AppError } from '../../errors';
import {
  Container, WhiteContainer, BackButton, BackButtonIcon,
  OptionalButton, OptionalText, LogoIcon
} from './styles';

type Props = {
  actionText?: string;
  actionIcon?: any;
  actionFunction?: ((input: any) => any) & (() => void);
  goBackFunction?: () => void;
  hasShadow?: boolean;
  barTextColor?: 'dark' | 'white';
}

export const Header = ({
  goBackFunction,
  actionFunction,
  actionText,
  actionIcon,
  hasShadow,
  barTextColor
}: Props) => {
  if (actionFunction) {
    if (actionText && actionIcon) throw new AppError(
      'You can not provide "actionText" and "actionIcon" at the same time.'
    );
  } else if (!actionFunction) {
    if (actionText || actionIcon) throw new AppError(
      'If you provide an "actionText" or "actionIcon", you must provide an "actionFunction" too.'
    );
  }

  const { goBack, canGoBack } = useNavigation();

  const handleGoBack = () => {
    if (goBackFunction) return goBackFunction();
    else return goBack();
  }

  return (
    <>
      <Container>
        <StatusBar
          barStyle={barTextColor === 'white' ? 'light-content' : 'dark-content'}
          backgroundColor="transparent"
          translucent
        />

        <WhiteContainer hasShadow={hasShadow}>
          {canGoBack() === true && (
            <BackButton onPress={handleGoBack}>
              <BackButtonIcon name="chevron-left" />
            </BackButton>
          )}
          {canGoBack() === false && (
            <BackButton onPress={() => {}}>
              <BackButtonIcon name="chevron-left" color="#FFF" />
            </BackButton>
          )}

          <LogoIcon />
          
          <OptionalButton onPress={actionFunction ? actionFunction : () => {}}>
            {actionText && <OptionalText>{actionText}</OptionalText>}
            {actionIcon && actionIcon}
          </OptionalButton>
        </WhiteContainer>
      </Container>
    </>
  );
}