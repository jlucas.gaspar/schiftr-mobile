import React from 'react';
import { StatusBar } from 'react-native';
import { Button } from '../../components';
import { GoogleIcon } from '../../assets/icons';
import { useGetStarted } from './hooks';
import {
  Container, Header, Subtitle, Regular, Strong, AuthButtons, Description, Footer, LoginLink, LoginText, LogoIcon
} from './styles';

export const GetStarted: React.FC = () => {
  const { handleGoogleSignUp, isLoading, goToSignInScreen, goToSignUpScreen } = useGetStarted();

  return (
    <Container>
      <StatusBar barStyle="light-content" backgroundColor="transparent" translucent />

      <Header>
        <LogoIcon />

        <Subtitle>
          <Regular>The only app</Regular>
          <Strong>you need</Strong>
        </Subtitle>
      </Header>

      <Description>
        Find friends around the world {'\n'}
        and share your journey
      </Description>

      <AuthButtons>
        {/* <Button isLoading={isLoading} text="SIGN UP WITH GOOGLE" onPress={handleGoogleSignUp} icon={GoogleIcon} /> */}
        <Button isLoading={isLoading} text="SIGN UP WITH GOOGLE" onPress={() => {}} icon={GoogleIcon} />
        <Button text="GET STARTED" onPress={goToSignUpScreen} />
      </AuthButtons>

      <Footer>
        <LoginLink onPress={goToSignInScreen}>
          <LoginText>I already have an account</LoginText>
        </LoginLink>
      </Footer>
    </Container>
  );
}