export class BaseError {
  constructor(
    public message: string,
    public type: string,
    public isAppError: boolean
  ) {}
}