import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getJwtToken, getUser } from '../utils/asyncStorage';
import * as userActions from '../store/actions/user';
import { RootState } from '../store';

export const useRooutRouter = () => {
  const { isAuthenticated } = useSelector((state: RootState) => state.user);
  const [isLoading, setLoading] = useState(true);
  const dispatch = useDispatch();

  useEffect(() => {
    const handleGetUser = async () => {
      const token = await getJwtToken();
      const user = await getUser();
      if (token && user) dispatch(userActions.authenticate({ token, user }));
      setLoading(false);
    }

    handleGetUser();
  }, [getJwtToken, userActions, dispatch, getUser]);

  return { isLoading, isAuthenticated }
}