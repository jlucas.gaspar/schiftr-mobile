import React from 'react';
import { useNavigation } from '@react-navigation/core';
import { RoutesList } from '../../../routes/constants';

import { Image, Container } from './Photo.styles';

type Props = {
  id?: string;
  imageUri?: string;
}

const grayImageBackgroundUri = 'https://images.pexels.com/photos/242236/pexels-photo-242236.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500';

export const Photo = ({ id, imageUri }: Props) => {
  const { navigate } = useNavigation();

  const handlePress = () => {
    if (id) return navigate(RoutesList.PostDetails as never, { postId: id } as never);
    else return;
  }

  if (!imageUri) return <Image source={{ uri: grayImageBackgroundUri }} />;
  return (
    <Container onPress={handlePress}>
      <Image source={{ uri: imageUri }} />
    </Container>
  );
}