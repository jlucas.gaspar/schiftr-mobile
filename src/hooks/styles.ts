import { RFValue, RFPercentage } from 'react-native-responsive-fontsize';

export const useStyles = () => {
  const valuePx = (value: number) => {
    return `${RFValue(value)}px`;
  }

  const valuePercentage = (percentage: number) => {
    return `${RFPercentage(percentage)}px`;
  }

  return {
    valuePercentage,
    valuePx,
  }
}