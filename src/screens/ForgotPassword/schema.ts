import { SchemaOf, object, string, boolean, number } from 'yup';
import { Auth } from '../../types/Auth';

type FormData = Auth.FormData.ForgotPassword;

export const forgotPasswordSchema: SchemaOf<FormData> = object().shape({
  email: string().required('E-mail is required.').email('Invalid e-mail')
});