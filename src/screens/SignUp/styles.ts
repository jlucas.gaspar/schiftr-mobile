import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { signUpBackground } from '../../assets/images';

export const Container = styled.View`
  width: 100%;
  height: 100%;
  background-color: ${_ => _.theme.colors.background};
  color: ${_ => _.theme.colors.text};
`;

export const Title = styled.Text`
  font-family: ${_ => _.theme.fonts.nunito.medium};
  font-size: ${RFValue(30)}px;
  color: ${_ => _.theme.colors.text};
  margin-left: 20px;
`;

export const BikeBackground = styled.ImageBackground.attrs({ source: signUpBackground })`
  z-index: -100;
  margin-top: -250px;
  height: 400px;
`;